package com.gpluslife.customView;

import org.xmlpull.v1.XmlPullParser;

import com.gpluslife.R;

import android.R.integer;
import android.content.Context;
import android.content.res.ColorStateList;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SegmentView extends LinearLayout {
	private TextView textView1;
	private TextView textView2;

	public SegmentView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	
	public SegmentView(Context context) {
		super(context);
		init();
	}
		
	private void init() {
//		this.setLayoutParams(new LinearLayout.LayoutParams(dp2Px(getContext(), 60), LinearLayout.LayoutParams.WRAP_CONTENT));
		textView1 = new TextView(getContext());
		textView2 = new TextView(getContext());
		textView1.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT, 1));
		textView2.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT, 1));
		textView1.setText(R.string.function);
		textView2.setText(R.string.Record);
		
		textView1.setId(0);
		textView2.setId(1);
		
		XmlPullParser xrp = getResources().getXml(R.drawable.seg_text_color_selector);  
	    try {  
	        ColorStateList csl = ColorStateList.createFromXml(getResources(), xrp);  
	        textView1.setTextColor(csl);
	        textView2.setTextColor(csl);
	      } catch (Exception e) {  
	    } 
	    textView1.setGravity(Gravity.CENTER);
	    textView2.setGravity(Gravity.CENTER);
	    textView1.setPadding(3, 6, 3, 6);
	    textView2.setPadding(3, 6, 3, 6);
	    setSegmentTextSize(16);
		textView1.setBackgroundResource(R.drawable.transform_my_style);
		textView2.setBackgroundResource(R.drawable.transform_log_style);
		textView1.setSelected(true);
		this.removeAllViews();
		this.addView(textView1);
		this.addView(textView2);
		this.invalidate();
	/*	
		textView1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (textView1.isSelected()) {
					return;
				}
				textView1.setSelected(true);
				textView2.setSelected(false);
//				if (mListener != null) {
//					mListener.onClick(textView1);
//				}
			}
		});
		textView2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (textView2.isSelected()) {
					return;
				}
				textView2.setSelected(true);
				textView1.setSelected(false);
				if (mListener != null) {
					mListener.onClick(textView2);
				}
			}
		});
			*/
	}

	/**
	 * 设置字体大小 单位dip
	 */
	public void setSegmentTextSize(int dp) {
		textView1.setTextSize(TypedValue.COMPLEX_UNIT_DIP, dp);
		textView2.setTextSize(TypedValue.COMPLEX_UNIT_DIP, dp);
	}
	
	/**
	 * 设置文字
	 */
	public void setSegmentText(CharSequence text,int position) {
		if (position == 0) {
			textView1.setText(text);
		}
		if (position == 1) {
			textView2.setText(text);
		}
	}
	
	public TextView getTextView1()
	{
		return textView1;
	}
	
	public TextView getTextView2()
	{
		return textView2;
	}
	
	public  interface onSegmentViewClickListener extends OnClickListener{
		
		public void onSegmentViewClick(View v,int position);
	}
}
