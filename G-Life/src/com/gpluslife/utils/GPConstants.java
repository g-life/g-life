package com.gpluslife.utils;

public class GPConstants {
	public static final String DIR_NAME_HEAD_IMG = "head_image";
    public static final String DIR_NAME_SCALED_IMAGE = "scaled_image";
    
    public static final String LIST_ALL_SELECTED_IMAGES = "all_selected_images";//保存所有选中的路径，在选择页面进行遍历
    public static final String DIR_PATH = "dir_path";//保存目录路径
    public static final String LIST_IMAGES_NAME_FROM_DIR = "images_name_from_dir";//所点击的目录下所有图片的名字
    public static final String LIST_SELECTED_PATH = "selected_path";
    public static final String CURRENT_CLICK_POSITION = "current_position";
    public static final String LIST_DELETE_POSITION = "delete_position";//查看原图后删除位置的集合
    
	public static final int NAVIGATION_RIGHT_BUTTON = 1002;  // GPNavigationView 右边Button的id
	public static final int NAVIGATION_LEFT_BUTTON = 1003;    // GPNavigationView 左边Button的id
    
    public static final int DEBUG = 0;
    
    public static Boolean ISMAINHOST= true;
    
}
