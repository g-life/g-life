package com.gpluslife.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.gpluslife.filemanager.GPFileManager;

import android.R.string;
import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;
import android.os.Environment;
import android.util.Base64;

@SuppressLint("NewApi") public class GPBitmapUtils {

	/*
		public static String getThumbUploadPath(String oldPath, int bitmapMaxWidth)
				throws Exception {
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(oldPath, options);
			int height = options.outHeight;
			int width = options.outWidth;
			int reqHeight = 0;
			int reqWidth = bitmapMaxWidth;
			reqHeight = (reqWidth * height) / width;
			options.inSampleSize = calculateInSampleSize(options, bitmapMaxWidth,
					reqHeight);
			options.inJustDecodeBounds = false;
			Bitmap bitmap = BitmapFactory.decodeFile(oldPath, options);
			Bitmap bbb = compressImage(Bitmap.createScaledBitmap(bitmap,
					bitmapMaxWidth, reqHeight, false));
			String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")

			.format(new Date());

			return GPBitmapUtils.saveImg(bbb, GPUtils.md5(timeStamp));

		}

		private static int calculateInSampleSize(BitmapFactory.Options options,int reqWidth, int reqHeight) {

			final int height = options.outHeight;
			final int width = options.outWidth;
			int inSampleSize = 1;
			if (height > reqHeight || width > reqWidth) {
				if (width > height) {
					inSampleSize = Math.round((float) height / (float) reqHeight);
				} else {
					inSampleSize = Math.round((float) width / (float) reqWidth);
				}
			}

			return inSampleSize;

		}
*/
		private static Bitmap compressImage(Bitmap image) {

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			image.compress(Bitmap.CompressFormat.JPEG, 80, baos);// ����ѹ������������100��ʾ��ѹ������ѹ�������ݴ�ŵ�baos��
			int options = 100;
			while (baos.toByteArray().length / 1024 > 100) { // ѭ���ж����ѹ����ͼƬ�Ƿ����100kb,���ڼ���ѹ��
				options -= 10;// ÿ�ζ�����10
				baos.reset();// ����baos�����baos
				image.compress(Bitmap.CompressFormat.JPEG, options, baos);// ����ѹ��options%����ѹ�������ݴ�ŵ�baos��
			}
			ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());// ��ѹ��������baos��ŵ�ByteArrayInputStream��
			Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, null);// ��ByteArrayInputStream������ͼƬ
			return bitmap;

		}
		
		 public static void saveImgToDirectory(Bitmap b,String name)
		 {
//			 String[] files = name.split(File.separator);
//			String nameString = files[files.length -1];
//			String tempNameString = name.replace(nameString, "");
//			 String path = GPFileManager.getFileManager().createFilePath(tempNameString);
//			 path += nameString;
			 String path = name;
			 File mediaFile = new File(path);
			 FileOutputStream fos = null ;
	         try {
				mediaFile.createNewFile();
				  fos = new FileOutputStream(mediaFile);
			      b.compress(Bitmap.CompressFormat.PNG, 100, fos);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	         finally
	         {
	             try {
	            	 if (fos != null) {
	 					fos.flush();
						 fos.close();
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		         b.recycle();
		         b = null;
		         System.gc();
	         }
	   	 }
		 
		 public static byte[] getBitmapByteFromDirectory(String filePath)
		 {
			 InputStream in = null;
			 byte by[]=null;
			 File f = new File(filePath);
			 try {
				in = new FileInputStream(f);
				 by=new byte[(int)f.length()]; 
				 in.read(by);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (IOException e) {
				// TODO: handle exception
			}
			 finally
			 {
				 if (in != null) {
					 try {
						in.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			 }
			 return by;
		 }
		 
		 public static Bitmap getBitmapFromDirectory(String filePath)
		 {
			 Bitmap bitmap = null;
			 File mediaFile = new File(filePath);
			 if (mediaFile.exists())
			 {
//				 ByteArrayOutputStream baos = new ByteArrayOutputStream();
				 
				bitmap = BitmapFactory.decodeFile(filePath);
			}
			 return bitmap;
		 }
	
	/**
	 * 		根据base64生成图片
	 */
	public static Bitmap stringtoBitmap(String base64string) {
		Bitmap bitmap = null;
		try {
			byte[] bitmapArray;
			bitmapArray = Base64.decode(base64string, Base64.DEFAULT);
			bitmap = BitmapFactory.decodeByteArray(bitmapArray, 0,
					bitmapArray.length);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bitmap;
	}
	
	public static Bitmap toRoundBitmap(Bitmap bitmap) {
		int width = bitmap.getWidth();
		int height = bitmap.getHeight();
		float roundPx;
		float left, top, right, bottom, dst_left, dst_top, dst_right, dst_bottom;
		if (width <= height) {
			roundPx = width / 2;

			left = 0;
			top = 0;
			right = width;
			bottom = width;

			height = width;

			dst_left = 0;
			dst_top = 0;
			dst_right = width;
			dst_bottom = width;
		} else {
			roundPx = height / 2;

			float clip = (width - height) / 2;

			left = clip;
			right = width - clip;
			top = 0;
			bottom = height;
			width = height;

			dst_left = 0;
			dst_top = 0;
			dst_right = height;
			dst_bottom = height;
		}

		Bitmap output = Bitmap.createBitmap(width, height, Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final Paint paint = new Paint();
		final Rect src = new Rect((int) left, (int) top, (int) right,
				(int) bottom);
		final Rect dst = new Rect((int) dst_left, (int) dst_top,
				(int) dst_right, (int) dst_bottom);
		final RectF rectF = new RectF(dst);

		paint.setAntiAlias(true);// 设置画笔无锯齿

		canvas.drawARGB(0, 0, 0, 0); // 填充整个Canvas

		// 以下有两种方法画圆,drawRounRect和drawCircle
		canvas.drawRoundRect(rectF, roundPx, roundPx, paint);// 画圆角矩形，第一个参数为图形显示区域，第二个参数和第三个参数分别是水平圆角半径和垂直圆角半径。
		// canvas.drawCircle(roundPx, roundPx, roundPx, paint);

		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));// 设置两张图片相交时的模式,参考
		canvas.drawBitmap(bitmap, src, dst, paint); // 以Mode.SRC_IN模式合并bitmap和已经draw了的Circle

		return output;
	}
	
}
