package com.gpluslife.utils;

import android.content.Context;
import android.os.Looper;
import android.widget.Toast;

public class GPErrorStlye {

//	private int code;
//	Context context;

	public static void stlye(Context context, int code) {
//		this.code = codes;
//		this.context = contexts;

		if (code == 0) {
			Looper.prepare();
			Toast.makeText(context, "时间戳有误！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 1) {
			Looper.prepare();
			Toast.makeText(context, "服务不可用！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 2) {
			Looper.prepare();
			Toast.makeText(context, "开发者权限不足！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 3) {
			Looper.prepare();
			Toast.makeText(context, "用户权限不足！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 4) {
			Looper.prepare();
			Toast.makeText(context, "图片上传失败！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 5) {
			Looper.prepare();
			Toast.makeText(context, "HTTP方法被禁止！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 6) {
			Looper.prepare();
			Toast.makeText(context, "编码错误！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 7) {
			Looper.prepare();
			Toast.makeText(context, "请求被禁止！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 8) {
			Looper.prepare();
			Toast.makeText(context, "服务已经作废！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 9) {
			Looper.prepare();
			Toast.makeText(context, "业务逻辑出错！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 20) {
			Looper.prepare();
			Toast.makeText(context, "缺少sesionid参数！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 21) {
			Looper.prepare();
			Toast.makeText(context, "账户验证过期，请重新登录！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 22) {
			Looper.prepare();
			Toast.makeText(context, "缺少appKey参数！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 23) {
			Looper.prepare();
			Toast.makeText(context, "无效的appKey参数！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 24) {
			Looper.prepare();
			Toast.makeText(context, "缺少签名参数！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 25) {
			Looper.prepare();
			Toast.makeText(context, "无效签名！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 26) {
			Looper.prepare();
			Toast.makeText(context, "缺少方法名参数！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 27) {
			Looper.prepare();
			Toast.makeText(context, "不存在的方法名！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 28) {
			Looper.prepare();
			Toast.makeText(context, "缺少版本参数！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 29) {
			Looper.prepare();
			Toast.makeText(context, "非法的版本参数！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 30) {
			Looper.prepare();
			Toast.makeText(context, "不支持的版本号！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 31) {
			Looper.prepare();
			Toast.makeText(context, "无效报文格式类型！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 32) {
			Looper.prepare();
			Toast.makeText(context, "缺少必选参数！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 33) {
			Looper.prepare();
			Toast.makeText(context, "非法的参数！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 34) {
			Looper.prepare();
			Toast.makeText(context, "用户调用服务的次数超限！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 35) {
			Looper.prepare();
			Toast.makeText(context, "会话调用服务的次数超限！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 36) {
			Looper.prepare();
			Toast.makeText(context, "应用调用服务的次数超限！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 37) {
			Looper.prepare();
			Toast.makeText(context, "应用调用服务的频率超限！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 100001) {
			Looper.prepare();
			Toast.makeText(context, "用户不存在！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 100002) {
			Looper.prepare();
			Toast.makeText(context, "帐号或密码错误！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 100003) {
			Looper.prepare();
			Toast.makeText(context, "用户存在！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 100004) {
			Looper.prepare();
			Toast.makeText(context, "验证码错误！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 100005) {
			Looper.prepare();
			Toast.makeText(context, "验证码过期！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 100006) {
			Looper.prepare();
			Toast.makeText(context, "没有头像！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 100010) {
			Looper.prepare();
			Toast.makeText(context, "数据格式有误！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 100011) {
			Looper.prepare();
			Toast.makeText(context, "没有家庭列表！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 100012) {
			Looper.prepare();
			Toast.makeText(context, "成员已经存在！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 100013) {
			Looper.prepare();
			Toast.makeText(context, "家庭成员账户不存在！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 101001) {
			Looper.prepare();
			Toast.makeText(context, "没有数据！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 101003) {
			Looper.prepare();
			Toast.makeText(context, "没数据！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 102001) {
			Looper.prepare();
			Toast.makeText(context, "没有数据！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 102002) {
			Looper.prepare();
			Toast.makeText(context, "帐号或密码错误！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 102003) {
			Looper.prepare();
			Toast.makeText(context, "没有这个日志，无法评论！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
		if (code == 102004) {
			Looper.prepare();
			Toast.makeText(context, "没有这张图片！", Toast.LENGTH_SHORT).show();
			Looper.loop();
		}
	}
}
