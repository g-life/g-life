package com.gpluslife.login;

import com.gpluslife.R;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

public class GPLoginLoadingDialog extends Dialog {
	private TextView tv;

	public GPLoginLoadingDialog(Context context) {
		super(context, R.style.loadingDialogStyle);
	}

	private GPLoginLoadingDialog(Context context, int theme) {
		super(context, theme);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_dialog_loading);
		tv = (TextView)this.findViewById(R.id.tv);
		tv.setText("正在登录...");
		LinearLayout linearLayout = (LinearLayout)this.findViewById(R.id.LinearLayout);
		linearLayout.getBackground().setAlpha(210);
	}
}
