package com.gpluslife.login.model;

import java.util.Map;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.gpluslife.utils.GPUtils;

/**
 * 
 * @author kevin
 * 
 *         登录后，用户信息解析及保存
 * 
 */

public class GPUserModelManager {

	/**
	 * 解析登录后返回的json字符串，并封装成user model对象
	 * 
	 * @param jsonString
	 * @return
	 */

	public static GPLoginUserModel parserJsonString(Activity activity,String jsonString , String username ,Boolean isMainHost)
	{
//		GPLoginUserModel currentUserModel = GPLoginUserModel.get();
		
		GPLoginUserModel userModel = new GPLoginUserModel();
		userModel.setUserName(username);
		Map<String, Object> resultMap = GPUtils.getMapForJson(jsonString);
		userModel.setAccountName((String) resultMap.get("accountName"))  ;
		userModel.setSessionId( (String) resultMap.get("sessionId"));
		userModel.setDeviceKey(resultMap.get("deviceKey").toString())  ;
		Map<String, Object> userMap = null ;
		if (resultMap.containsKey("user"))
		{
			String userString = resultMap.get("user").toString();
			userMap = GPUtils.getMapForJson(userString);
		}
		if (userMap.containsKey("info"))
		{
			String userinfoString =  userMap.get("info").toString();
			Map<String, Object>  userInfoMap = GPUtils.getMapForJson(userinfoString);
			userModel.setNickname(userInfoMap.get("nickname").toString());
			userModel.setSignature(userInfoMap.get("signature").toString());
			userModel.setGender( Integer.parseInt(userInfoMap.get("gender").toString()));
			userModel.setBirthday(userInfoMap.get("birthday").toString());
			userModel.setDistrict(userInfoMap.get("district").toString());
		}

		if (userMap.containsKey("icon")) {
			String userString = userMap.get("icon").toString();
			Map<String, Object> iconMap = GPUtils.getMapForJson(userString);
			userModel.setIconMD5( iconMap.get("md5Code").toString());
		}

		if (userMap.containsKey("qRCode")) {
			String userString =  userMap.get("qRCode").toString();
			Map<String, Object> qRCodeMap = GPUtils.getMapForJson(userString);
			userModel.setqRCodeMD5(qRCodeMap.get("md5Code").toString());
		}

		GPLoginUserModel tempUserModel = GPUserModelManager.getUserModelFromSharePreferences(activity,isMainHost);
		if (tempUserModel == null || tempUserModel.getSessionId() == null)
		{
			userModel.setIsDownloadIcon(true);
			userModel.setIsDownloadQRcode(true) ;
//			currentUserModel.setModel(userModel);
			GPLoginUserModel.setModel(userModel);
//			GPLoginUserModel.get().setIsDownloadIcon(true);
//			GPLoginUserModel.get().setIsDownloadQRcode(true) ;
		}
		else 
		{
			Boolean isDownloadIcon ;
			Boolean isDownloadQRcode ;
			if (tempUserModel.getIconMD5() == null || !tempUserModel.getIconMD5().equals(userModel.getIconMD5()))
			{
				isDownloadIcon = true;
			}
			else 
			{
				isDownloadIcon = false;
			}
			if (tempUserModel.getqRCodeMD5() == null || (!tempUserModel.getqRCodeMD5().equals(userModel.getqRCodeMD5())))
			{
				isDownloadQRcode = true;
			}
			else 
			{
				isDownloadQRcode = false;
			}
			userModel.setIsDownloadIcon(isDownloadIcon);
			userModel.setIsDownloadQRcode(isDownloadQRcode) ;
			GPLoginUserModel.setModel(userModel);
		}
		

		return userModel;
	}

	/**
	 * 保存登录的用户信息
	 * 
	 * @param activity
	 * @param userModel
	 * @param password
	 * @param username
	 */
	public static void saveUserModelToSharePreferences(Activity activity,
			GPLoginUserModel userModel, String password) {
		SharedPreferences sp = activity.getSharedPreferences("usermodel",
				activity.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putString("sessionId", userModel.getSessionId());
		editor.putString("password", password);
		editor.putString("qRCodeMD5", userModel.getqRCodeMD5());
		editor.putString("iconMD5", userModel.getIconMD5());
		editor.putString("nickname", userModel.getNickname());
		editor.putString("signature", userModel.getSignature());
		editor.putInt("gender", userModel.getGender());
		editor.putString("birthday", userModel.getBirthday());
		editor.putString("district", userModel.getDistrict());
		editor.putString("username", userModel.getUserName());
		editor.putString("accountname", userModel.getAccountName());
		editor.putString("devicekey", userModel.getDeviceKey());
		editor.commit();
	}
	
	/**
	 * 获取保存的用户信息
	 * 
	 * @param activity
	 * @param userModel
	 * @param password
	 * @param username
	 */
	public static GPLoginUserModel getUserModelFromSharePreferences(Activity activity,boolean isMainHost)
	{
		SharedPreferences sp  ;
		if (isMainHost == true)
		{
			sp = activity.getSharedPreferences("usermodel",
					activity.MODE_PRIVATE);
		}
		else {
			sp = activity.getSharedPreferences("tempusermodel",
					activity.MODE_PRIVATE);
		}
		 
//		GPLoginUserModel currentUserModel = GPLoginUserModel.get();
		GPLoginUserModel currentUserModel = new GPLoginUserModel();
		currentUserModel.setSessionId(sp.getString("sessionId", null));
		currentUserModel.setAccountName(sp.getString("accountname", null));
		currentUserModel.setPassword(sp.getString("password", null));
		currentUserModel.setqRCodeMD5(sp.getString("qRCodeMD5", null));
		currentUserModel.setIconMD5(sp.getString("iconMD5", null));
		currentUserModel.setGender(sp.getInt("gender", 0));
		currentUserModel.setNickname(sp.getString("nickname", null));
		currentUserModel.setSignature(sp.getString("signature", null));
		currentUserModel.setBirthday(sp.getString("birthday", null));
		currentUserModel.setDistrict(sp.getString("district", null));
		currentUserModel.setUserName(sp.getString("username", null));
		currentUserModel.setDeviceKey(sp.getString("devicekey", null));
		currentUserModel.setIsDownloadIcon(false);
		currentUserModel.setIsDownloadQRcode(false);
		return currentUserModel;
	}

	/**
	 *  保存切换账号后的用户信息
	 *  @param  activity
	 *  @param  userModel
	 *  @param password
	 */
	public static void saveTempUserModelToSharePreferences(Activity activity,
			GPLoginUserModel userModel, String password) {
		SharedPreferences sp = activity.getSharedPreferences("tempusermodel",
				activity.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putString("sessionId", userModel.getSessionId());
		editor.putString("password", password);
		editor.putString("qRCodeMD5", userModel.getqRCodeMD5());
		editor.putString("iconMD5", userModel.getIconMD5());
		editor.putString("nickname", userModel.getNickname());
		editor.putString("signature", userModel.getSignature());
		editor.putInt("gender", userModel.getGender());
		editor.putString("birthday", userModel.getBirthday());
		editor.putString("district", userModel.getDistrict());
		editor.putString("username", userModel.getUserName());
		editor.putString("accountname", userModel.getAccountName());

		editor.commit();
	}
	
	/**
	 * 获取切换账号后的用户信息
	 * 
	 * @param activity
	 * @param userModel
	 * @param password
	 * @param username
	 */
	public static GPLoginUserModel getTempUserModelFromSharePreferences(Activity activity)
	{
		SharedPreferences sp = activity.getSharedPreferences("tempusermodel",
				activity.MODE_PRIVATE);
//		GPLoginUserModel currentUserModel = GPLoginUserModel.get();
		GPLoginUserModel currentUserModel = new GPLoginUserModel();
		currentUserModel.setSessionId(sp.getString("sessionId", null));
		currentUserModel.setAccountName(sp.getString("accountname", null));
		currentUserModel.setqRCodeMD5(sp.getString("qRCodeMD5", null));
		currentUserModel.setIconMD5(sp.getString("iconMD5", null));
		currentUserModel.setGender(sp.getInt("gender", 0));
		currentUserModel.setNickname(sp.getString("nickname", null));
		currentUserModel.setSignature(sp.getString("signature", null));
		currentUserModel.setBirthday(sp.getString("birthday", null));
		currentUserModel.setDistrict(sp.getString("district", null));
		currentUserModel.setUserName(sp.getString("username", null));
		currentUserModel.setIsDownloadIcon(false);
		currentUserModel.setIsDownloadQRcode(false);
		return currentUserModel;
	}
	
	public static void destroyUserModel(Activity activity,String key)
	{
		SharedPreferences sp = activity.getSharedPreferences(key,
				activity.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.clear();
		editor.commit();
	}
	
	public static void setIsMainHost(Activity activity,Boolean isMainHost) 
	{
		SharedPreferences sp = activity.getSharedPreferences("ismainhost",
				activity.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putBoolean("mainhost", isMainHost);
		editor.commit();
	}
	
	public static Boolean getIsMainHost(Activity activity) 
	{
		SharedPreferences sp = activity.getSharedPreferences("ismainhost",
				activity.MODE_PRIVATE);
		return sp.getBoolean("mainhost", true);
	}
}
