package com.gpluslife.home.note;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.gpluslife.R;
import com.gpluslife.customView.GPNavigationView;
import com.gpluslife.entity.ImageBean;
import com.gpluslife.home.note.adapter.ImageDirsAdapter;
import com.gpluslife.utils.GPConstants;

public class NotePicActivity extends Activity {

	GridView mGroupGridView;
	private ImageDirsAdapter mAdapter;// 自定义的适配器
	private GPNavigationView navigationView;
	private Button back;
	
	private static final String TAG = "ShowImageDirsActivity";
    private final static int SCAN_OK = 1;
    private final int FLAG = 2;
    //以目录名为Key，该目录下图片的路径为值
    private HashMap<String, List<String>> mGroupMap = new HashMap<String, List<String>>();
    private List<ImageBean> mList = new ArrayList<ImageBean>();
    private ProgressDialog mProgressDialog;
	private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case SCAN_OK:
                    finishScanImage();
                    break;
            }
        }
    };
    
    private void finishScanImage() {
        //关闭进度条
        mProgressDialog.dismiss();
        mList = subGroupOfImage(mGroupMap);
        if (mList.isEmpty()) {
            Toast.makeText(this, "没有找到可选图片。", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        mAdapter = new ImageDirsAdapter(this, mList, mGroupGridView);
        mGroupGridView.setAdapter(mAdapter);
    }
    
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_image_bucket);

		/*helper = AlbumHelper.getHelper();
		helper.init(getApplicationContext());*/

		initView();
		getImages();

        final ArrayList<String> paths = getIntent().getStringArrayListExtra(GPConstants.LIST_ALL_SELECTED_IMAGES);

        mGroupGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                String parentPath = mList.get(position).getFolderPath();
                List<String> childList = mGroupMap.get(parentPath);
                Intent intent = new Intent(NotePicActivity.this, ShowImagesActivity.class);
                //把已经选择好的照片和拍照的照片的路径传递到ShowImagesActivity,显示勾选状态
                intent.putStringArrayListExtra(GPConstants.LIST_ALL_SELECTED_IMAGES, paths);
                //当前选中目录的路径
                intent.putExtra(GPConstants.DIR_PATH, parentPath);
                //当前选中目录下所有图片的图片名
                intent.putStringArrayListExtra(GPConstants.LIST_IMAGES_NAME_FROM_DIR, (ArrayList<String>) childList);
                startActivityForResult(intent, FLAG);
            }
        });
	}

	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == FLAG) {
            setResult(RESULT_OK, data);
            finish();
        }
    }
	
	/**
     * 利用ContentProvider扫描手机中的图片，此方法在运行在子线程中
     */
    private void getImages() {
       /* if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            Toast.makeText(this, R.string.no_sdCard, Toast.LENGTH_SHORT).show();
            return;
        }*/

        //显示进度条
        mProgressDialog = ProgressDialog.show(this, null, "正在加载" );

        new Thread(new Runnable() {

            @Override
            public void run() {
                Uri mImageUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                ContentResolver contentResolver = NotePicActivity.this.getContentResolver();
                //只查询jpeg,jpg和png的图片
                Cursor cursor = contentResolver.query(mImageUri, null,
                        MediaStore.Images.Media.MIME_TYPE + "=? or "
                                + MediaStore.Images.Media.MIME_TYPE + "=? or " + MediaStore.Images.Media.MIME_TYPE + "=?",
                        new String[]{"image/jpeg", "image/png", "image/jpg"}, MediaStore.Images.Media.DATE_MODIFIED + " desc"
                );

                while (cursor.moveToNext()) {
                    //获取图片的路径
                    String path = cursor.getString(cursor
                            .getColumnIndex(MediaStore.Images.Media.DATA));
                    //获取该图片的父路径名
                    String parentPath = new File(path).getParentFile().getAbsolutePath();
                    String imageName = cursor.getString(cursor
                            .getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME));
                    String imageSize = cursor.getString(cursor
                            .getColumnIndex(MediaStore.Images.Media.SIZE));

                    //根据父路径名将图片放入到mGroupMap中
                    if (!mGroupMap.containsKey(parentPath)) {
                        List<String> childList = new ArrayList<String>();
                        if (Integer.parseInt(imageSize) > 20) {
                            childList.add(imageName);
                        }
                        mGroupMap.put(parentPath, childList);
                    } else {
                        if (Integer.parseInt(imageSize) > 20) {
                            mGroupMap.get(parentPath).add(imageName);
                        }
                    }
                }
                cursor.close();

                //通知Handler扫描图片完成
                mHandler.sendEmptyMessage(SCAN_OK);

            }
        }).start();

    }
	
    /**
     * 组装分组界面GridView的数据源，因为我们扫描手机的时候将图片信息放在HashMap中
     * 所以需要遍历HashMap将数据组装成List
     *
     * @param mGroupMap
     * @return
     */
    private List<ImageBean> subGroupOfImage(HashMap<String, List<String>> mGroupMap) {
        if (mGroupMap.size() == 0) {
            return mList;
        }

        for (String key : mGroupMap.keySet()) {
            ImageBean mImageBean = new ImageBean();
            mImageBean.setFolderName(getParentName(key));
            mImageBean.setTopImagePath(mGroupMap.get(key).get(0));
            mImageBean.setImageCounts(mGroupMap.get(key).size());
            mImageBean.setFolderPath(key);
            mList.add(mImageBean);
        }
        return mList;
    }

    private String getParentName(String parentPath) {
        if ((null != parentPath) && (!"".equals(parentPath))) {
            int index = parentPath.lastIndexOf("/");
            return parentPath.substring(index + 1);
        }
        return "";
    }
    

	/**
	 * 初始化view视图
	 */
	private void initView() {
		mGroupGridView = (GridView) findViewById(R.id.gridview);
		mGroupGridView.setAdapter(mAdapter);
		navigationView = (GPNavigationView) findViewById(R.id.bucketNavigationView);
		back = navigationView.getBtn_left();
		back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
//				Intent intent=new Intent().setClass(NotePicActivity.this, NoteActivity.class);
//				startActivity(intent);
//				finish();
				onBackPressed();
			}
		});
	}
}
