package com.gpluslife.home.note;

import java.util.ArrayList;

import uk.co.senab.photoview.PhotoView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.gpluslife.R;
import com.gpluslife.utils.GPConstants;
import com.gpluslife.utils.NativeImageLoader;

public class PhotoActivity extends Activity {

	private ViewPager pager;
	private MyPageAdapter adapter;
	private ArrayList<String> mSelectedImgsPath;
	private int mCurrentPosition;
	private ArrayList<Integer> mDeletePosition = new ArrayList<Integer>();//存储所删除图片的位置

	RelativeLayout photo_relativeLayout;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_photo);

		photo_relativeLayout = (RelativeLayout) findViewById(R.id.photo_relativeLayout);
		photo_relativeLayout.setBackgroundColor(0x70000000);

		mSelectedImgsPath = getIntent().getStringArrayListExtra(GPConstants.LIST_SELECTED_PATH);
		mCurrentPosition = getIntent().getIntExtra(GPConstants.CURRENT_CLICK_POSITION, 0);
		Button photo_bt_exit = (Button) findViewById(R.id.photo_bt_exit);
		photo_bt_exit.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				finish();
			}
		});
		Button photo_bt_del = (Button) findViewById(R.id.photo_bt_del);
		photo_bt_del.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				mSelectedImgsPath.remove(mCurrentPosition);
				mDeletePosition.add(mCurrentPosition);
				adapter = new MyPageAdapter(mSelectedImgsPath);
				pager.setAdapter(adapter);
				if (mSelectedImgsPath.size() != 0) {
                    if (mCurrentPosition < mSelectedImgsPath.size()) {
                        pager.setCurrentItem(mCurrentPosition);
                    } else {
                        mCurrentPosition = 0;
                    }
                } else {
                    /**
                     * 没有图片了，则返回添加界面
                     */
                    backForeActivity();
                }
			}
		});
		Button photo_bt_enter = (Button) findViewById(R.id.photo_bt_enter);
		photo_bt_enter.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				backForeActivity();
			}
		});
		
		pager = (ViewPager) findViewById(R.id.viewpager);

		adapter = new MyPageAdapter(mSelectedImgsPath);// 构造adapter
		pager.setAdapter(adapter);// 设置适配器
		pager.setCurrentItem(mCurrentPosition);
		pager.setOnPageChangeListener(pageChangeListener);
	}
	
	private void backForeActivity(){
		Intent intent = new Intent();
		intent.putIntegerArrayListExtra(GPConstants.LIST_DELETE_POSITION, mDeletePosition);
		setResult(RESULT_OK,intent);
		finish();
	}


	private OnPageChangeListener pageChangeListener = new OnPageChangeListener() {

		public void onPageSelected(int arg0) {// 页面选择响应函数
			mCurrentPosition = arg0;
		}

		public void onPageScrolled(int arg0, float arg1, int arg2) {// 滑动中。。。

		}

		public void onPageScrollStateChanged(int arg0) {// 滑动状态改变

		}
	};

	class MyPageAdapter extends PagerAdapter {

		private ArrayList<String> listViews;// content
		private int size;// 页数
		private int mScreenWidth;
		private int mScreenHeight;
		private LayoutInflater mInflater;
		private Bitmap mBitmap;
		public MyPageAdapter(ArrayList<String> lt) {// 构造函数
			this.listViews = lt;
			size = listViews == null ? 0 : listViews.size();
			mInflater = LayoutInflater.from(PhotoActivity.this);
			DisplayMetrics outMetrics = new DisplayMetrics();
	        getWindowManager().getDefaultDisplay().getMetrics(outMetrics);
	        mScreenWidth = outMetrics.widthPixels;
	        mScreenHeight = outMetrics.heightPixels;
		}
		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			// TODO Auto-generated method stub
			 final View view = mInflater.inflate(R.layout.photoview_image_layout, null);
		        final PhotoView imgView = (PhotoView) view.findViewById(R.id.id_image);
		        //RelativeLayout layout= (RelativeLayout) view.findViewById(R.id.choose_image_buttom);
		        String path = mSelectedImgsPath.get(position);
		        imgView.setTag(path);
		        mBitmap = NativeImageLoader.getInstance().loadNativeImage(path, mScreenWidth, mScreenHeight, new NativeImageLoader.NativeImageCallBack() {
		            @Override
		            public void onImageLoader(Bitmap mBitmap, String path) {
		                if (null != imgView) {
		                    imgView.setImageBitmap(mBitmap);
		                }
		            }
		        });
		        if (mBitmap == null) {
		            mBitmap = BitmapFactory.decodeResource(PhotoActivity.this.getResources(), R.drawable.picture_no);
		        } else {
		            imgView.setImageBitmap(mBitmap);
		        }
		        container.addView(view);
		        return view;
		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return size;
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			// TODO Auto-generated method stub
			return arg0==arg1;
		}
		@Override
	    public void destroyItem(ViewGroup container, int position, Object object) {
	        View view = (View) object;
	        container.removeView(view);
	    }
	}
}
