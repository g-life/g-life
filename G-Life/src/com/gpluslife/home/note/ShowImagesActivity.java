package com.gpluslife.home.note;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.gpluslife.R;
import com.gpluslife.home.note.adapter.ShowImagesAdapter;
import com.gpluslife.utils.GPConstants;

public class ShowImagesActivity extends Activity {
    private GridView mGridView;
    private TextView undo;
    private Button bt;
    private List<String> mImagesName; //得到Intent存储的图片名字
    private ArrayList<String> mSelectedImages;//所有已选择的图片路径
    private ShowImagesAdapter mAdapter;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {

            if (msg != null) {
                if (msg.what == -1) {
                    Toast.makeText(ShowImagesActivity.this, "最多只能选择9张", Toast.LENGTH_SHORT).show();
                }
                if (msg.obj != null) {
                    if (null != bt) {
                        bt.setEnabled(true);
                        bt.setText(getResources().getString(R.string.GPLUS_COMMON_DONE) + "(" + msg.obj.toString() + ")");
                    }
                }
            }
        }

        ;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_grid);


        mImagesName = getIntent().getStringArrayListExtra(GPConstants.LIST_IMAGES_NAME_FROM_DIR);
        String dirPath = getIntent().getStringExtra(GPConstants.DIR_PATH);
        //已经选择好的照片和拍照的照片的路径,显示勾选状态
        mSelectedImages = getIntent().getStringArrayListExtra(GPConstants.LIST_ALL_SELECTED_IMAGES);

        findViews();

        mAdapter = new ShowImagesAdapter(this, mImagesName, mSelectedImages, dirPath,mGridView, mHandler);
        mGridView.setAdapter(mAdapter);
        
        undo=(TextView) findViewById(R.id.undo);
		undo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		bt = (Button) findViewById(R.id.bt);
		bt.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				List<String> paths = mAdapter.getSelectMap();

                Intent intent = new Intent();
                intent.putStringArrayListExtra(GPConstants.LIST_SELECTED_PATH, (ArrayList) paths);
                setResult(RESULT_OK, intent);
                finish();
                mAdapter.getSelectMap().clear();
			}
		});
	}

    private void findViews() {
        mGridView = (GridView) findViewById(R.id.gridview);
    }
    
    @Override
    protected void onResume() {
    	// TODO Auto-generated method stub
    	if(mSelectedImages==null){
    		bt.setText(getResources().getString(R.string.GPLUS_COMMON_DONE) + "(" + mAdapter.getSelectMap().size() + "/9)");
    	}else{
    		bt.setText(getResources().getString(R.string.GPLUS_COMMON_DONE) + "(" + (mAdapter.getSelectMap().size()+mSelectedImages.size()) + "/9)");
    	}
    	super.onResume();
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.show_images, menu);
        MenuItem item = menu.findItem(R.id.complete);
        View view = item.getActionView();

        assert view != null;
        mButton = (Button) view.findViewById(R.id.confirm_button);
        if (mAdapter.getSelectItems().size() < 1) {
            mButton.setText(getResources().getString(R.string.confirm) + "(0/9)");
        } else {
            mButton.setText(getResources().getString(R.string.confirm) + "(" + mAdapter.getSelectItems().size() + "/9)");
        }
        //mButton.setEnabled(false);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (Map.Entry<String, Boolean> entry : mAdapter.getSelectMap().entrySet()) {
                    ShowImagesAdapter.mSelectMap.put(entry.getKey(), entry.getValue());
                }
                mAdapter.getSelectMap().clear();
                mButton.setEnabled(false);

                mTextView.getBackground().setAlpha(100);
                mTextView.setVisibility(View.VISIBLE);

                List<String> paths = null;


                if(null == mSelectedImages) {
                    paths = mAdapter.getSelectItems();
                }else{
                    paths = mSelectedImages;
                    if ((null != mAdapter.getSelectItems()) && mAdapter.getSelectItems().size()>0){
                        paths.addAll(mAdapter.getSelectItems());
                    }
                }

                Intent intent = new Intent();
                intent.putStringArrayListExtra(Constants.LIST_SELECTED_PATH, (ArrayList) paths);
                setResult(RESULT_OK, intent);
                finish();
                mAdapter.getSelectItems().clear();
            }
        });
        return super.onCreateOptionsMenu(menu);
    }*/


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //backActivity();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*private void backActivity() {
        mAdapter.getSelectMap().clear();
        finish();
    }*/

   /* @Override
    public void onBackPressed() {
        backActivity();
    }*/

}
