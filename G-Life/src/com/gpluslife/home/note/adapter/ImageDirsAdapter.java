package com.gpluslife.home.note.adapter;

import java.io.File;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.gpluslife.R;
import com.gpluslife.entity.ImageBean;
import com.gpluslife.utils.NativeImageLoader;

public class ImageDirsAdapter extends BaseAdapter {
    protected LayoutInflater mInflater;
    private List<ImageBean> mList;
    private GridView mGridView;
    private int mImageViewWidth;//展示图片的ImageView的宽
    private int mImageViewHeight;//展示图片的ImageView的高

    public ImageDirsAdapter(Context context, List<ImageBean> mList, GridView mGridView) {
        this.mList = mList;
        this.mGridView = mGridView;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        ImageBean mImageBean = mList.get(position);
        String path = mImageBean.getFolderPath() + File.separator + mImageBean.getTopImagePath();
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.item_image_bucket, parent,false);
            viewHolder.mImageView = (ImageView) convertView.findViewById(R.id.image);
            viewHolder.mTextViewTitle = (TextView) convertView.findViewById(R.id.name);
            viewHolder.mTextViewCounts = (TextView) convertView.findViewById(R.id.count);
            /*viewHolder.mImageView.setOnMeasureListener(new ChoosePhotosImageView.OnMeasureListener() {
                @Override
                public void onMeasureSize(int width, int height) {
                    mImageViewHeight = height;
                    mImageViewWidth = width;
                }
            });*/
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            //viewHolder.mImageView.setImageResource(R.drawable.picture_no);
        }

        viewHolder.mTextViewTitle.setText(mImageBean.getFolderName());
        viewHolder.mTextViewCounts.setText(Integer.toString(mImageBean.getImageCounts()));
        //给ImageView设置路径Tag,这是异步加载图片的小技巧
        viewHolder.mImageView.setTag(path);


        //利用NativeImageLoader类加载本地图片
        Bitmap bitmap = NativeImageLoader.getInstance().loadNativeImage(path, mImageViewWidth, mImageViewHeight, new NativeImageLoader.NativeImageCallBack() {

            @Override
            public void onImageLoader(Bitmap bitmap, String path) {
                ImageView mImageView = (ImageView) mGridView.findViewWithTag(path);
                if (null != bitmap && null != mImageView) {
                    mImageView.setImageBitmap(bitmap);
                }
            }
        });

        if (bitmap != null) {
            viewHolder.mImageView.setImageBitmap(bitmap);
        } else {
            viewHolder.mImageView.setImageResource(R.drawable.picture_no);
        }
        return convertView;
    }


    public static class ViewHolder {
        public ImageView mImageView;
        public TextView mTextViewTitle;
        public TextView mTextViewCounts;
    }

}
