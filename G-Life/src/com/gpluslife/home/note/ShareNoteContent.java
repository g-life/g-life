package com.gpluslife.home.note;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.gpluslife.R;
import com.gpluslife.service.GPRequestService;
import com.gpluslife.service.http.GPClientCallBack;
import com.gpluslife.utils.GPUtils;

public class ShareNoteContent {

	protected static final String TAG = "ShareNoteContent";
	private String sessionId, content;
	private Activity activity;
	private ArrayList<String> list;
	private AnsyTry1 anys1 = null;
	private String logid;
	NoteUploadingPhotoLoadingDialog dialog;
	int count = 0;
	private ExecutorService executorService;
	public ShareNoteContent(){
		executorService = Executors.newFixedThreadPool(12);
	}
	public void notecontent(Activity activitys, String sessionIds,
			String contents, NoteUploadingPhotoLoadingDialog dialoges,
			ArrayList<String> lt) {
		this.sessionId = sessionIds;
		this.content = contents;
		this.activity = activitys;
		this.dialog = dialoges;
		// this.gridAdapter=adapter;
		list = lt;
		/*
		 * for (int i = 0; i < Bimp.drr.size(); i++) { String Str =
		 * Bimp.drr.get(i).substring( Bimp.drr.get(i).lastIndexOf("/") + 1,
		 * Bimp.drr.get(i).lastIndexOf(".")); Log.d(TAG, "path="+Str);
		 * list.add(Str); }
		 */

		count = 0;
		// 上传文字内容获取logid
		GPRequestService.get().uploadLogWithNote(sessionId, content,
				new GPClientCallBack() {
					public void onSuccess(String jsonString) throws Exception {
						Map<String, Object> map = GPUtils
								.getMapForJson(jsonString);
						int code = Integer.parseInt(map.get("code").toString());
						String logid = map.get("result").toString();
						Log.d(TAG, "code=" + code + "logid" + logid
								+ "listsize" + list.size());
						Log.d(TAG, "send Success!");
						if (list.size() != 0) {
							// 上传图片
							/*anys1 = new AnsyTry1(logid);
							anys1.execute();*/
							// notephoto(logid);
							UploadImages up = new UploadImages(logid);
							executorService.submit(up);
							
						} else {
							if (code == 200) {
								Looper.prepare();
								dialog.dismiss();
								Toast.makeText(activity, R.string.sendsuccues,
										Toast.LENGTH_LONG).show();
								Looper.loop();
							}
							// 跳转到LOg页面
						}
						if (code != 200) {
							Looper.prepare();
							Toast.makeText(activity, R.string.senddefeated,
									Toast.LENGTH_LONG).show();
							Looper.loop();
							Log.d(TAG, "send defeat!");
						}
					}
				});
	}

	

	public void notephoto(String logids) {
		Log.d(TAG, "logids=" + logids);
		for (String photoPath : list) {
			Log.d(TAG, "开始上传图片!");
			GPRequestService.get().uploadLogWithPhotos(sessionId, logids, photoPath,
					new GPClientCallBack() {
						@Override
						public void onSuccess(String jsonString)
								throws Exception {
							Map<String, Object> map = GPUtils
									.getMapForJson(jsonString);
							Log.e(TAG, "map=" + map.toString());
							int code = Integer.parseInt(map.get("code")
									.toString());
							Log.d(TAG, "code=" + code);
							String picid = map.get("result").toString();
							Log.e(TAG, "upload images id is " +picid + " and code is "+code);
							//Log.d(TAG, "code=" + code + "picid" + picid);
							if (code == 200) {
								++count;
								Log.e(TAG, "count     "+count);
								if (count == list.size()) {
									// 高清的压缩图片全部就在 list 路径里面了
									// 高清的压缩过的 bmp 对象 都在 Bimp.bmp里面
									// 完成上传服务器后deleteDir
									// NoteFileUtils.deleteDir();
									// gridAdapter.notifyDataSetChanged();
									// gridAdapter.update();
									Looper.prepare();
									dialog.dismiss();
									Toast.makeText(activity,
											R.string.sendsuccues,
											Toast.LENGTH_LONG).show();
									Looper.loop();

									// 跳转到LOg页面
								}
							}
							if (code != 200) {
								dialog.dismiss();
								// NoteFileUtils.deleteDir();
								Looper.prepare();
								Toast.makeText(activity, R.string.senddefeated,
										Toast.LENGTH_LONG).show();
								Looper.loop();
								Log.d(TAG, "send defeat!");
							}
						}
					});
		}
	}

	class UploadImages extends Thread{
		private String logId;

		public UploadImages(String logId) {
			this.logId = logId;
		}
		@Override
		public void run() {
			// TODO Auto-generated method stub
			notephoto(logId);
		}
	}
	
	class AnsyTry1 extends AsyncTask<Void, Integer, Float> {

		private String logId;

		public AnsyTry1(String logId) {
			this.logId = logId;
		}

		@Override
		protected Float doInBackground(Void... arg0) {
			notephoto(logId);
			return null;
		}
	}
}
