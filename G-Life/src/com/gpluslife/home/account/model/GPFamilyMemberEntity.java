package com.gpluslife.home.account.model;

public class GPFamilyMemberEntity extends Object {

	private String userName;
	private String accountName;
	private String nickName;
	private String relationShip;
	private String sha1PWD;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getRelationShip() {
		return relationShip;
	}
	public void setRelationShip(String relationShip) {
		this.relationShip = relationShip;
	}
	public String getSha1PWD() {
		return sha1PWD;
	}
	public void setSha1PWD(String sha1pwd) {
		sha1PWD = sha1pwd;
	}
	
	
	
}
