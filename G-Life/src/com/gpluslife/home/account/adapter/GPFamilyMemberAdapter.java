package com.gpluslife.home.account.adapter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.microedition.khronos.opengles.GL;

import org.json.JSONObject;

import com.gpluslife.R;
import com.gpluslife.R.id;
import com.gpluslife.filemanager.GPFileManager;
import com.gpluslife.home.account.GPBitmapLoaders;
import com.gpluslife.home.account.model.GPFamilyMemberEntity;
import com.gpluslife.login.model.GPLoginUserModel;
import com.gpluslife.service.GPRequestService;
import com.gpluslife.service.http.GPClientCallBack;
import com.gpluslife.utils.GPBitmapUtils;
import com.gpluslife.utils.GPUtils;

import android.R.integer;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Message;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class GPFamilyMemberAdapter extends BaseAdapter{

	private Context context;
	private GPBitmapLoaders bitmapLoaders;
	
	private List<GPFamilyMemberEntity> memberEntities = new ArrayList<GPFamilyMemberEntity>();
 	
	public List<GPFamilyMemberEntity> getMemberEntities() {
		return memberEntities;
	}

	public void setMemberEntities(List<GPFamilyMemberEntity> memberEntities) {
		this.memberEntities = memberEntities;
	}

	public GPFamilyMemberAdapter(Context ctx)
	{
		bitmapLoaders = new GPBitmapLoaders(ctx);
		context = ctx;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return memberEntities.size() + 1;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		if (memberEntities.size() != 0 && position != memberEntities.size()) 
		{
			return memberEntities.get(position);
		}
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		if (position == memberEntities.size())
		{
//			GPFamilyMemberEntity entity =	 (GPFamilyMemberEntity)getItem(position);
			convertView = View.inflate(context, R.layout.family_member_item, null);
			ImageView imgView = (ImageView)convertView.findViewById(R.id.user_icon);
			imgView.setImageResource(R.drawable.family_member_add);
			TextView usernameTV = (TextView)convertView.findViewById(R.id.phonenumber);
			usernameTV.setText(R.string.GPLUS_FAMILY_MEMBER_ADD);
			TextView nicknameTV = (TextView)convertView.findViewById(R.id.user_name);
			nicknameTV.setVisibility(View.GONE);
			LinearLayout.LayoutParams params=new LinearLayout.LayoutParams( 
					   LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT 
					  );
			params.gravity = Gravity.CENTER_VERTICAL;
			usernameTV.setLayoutParams(params);
			ImageView noteImageView = (ImageView)convertView.findViewById(R.id.login_note_icon);
			noteImageView.setVisibility(View.GONE);
		}
		else {
			GPFamilyMemberEntity entity =	 (GPFamilyMemberEntity)getItem(position);
			convertView = View.inflate(context, R.layout.family_member_item, null);
			TextView usernameTV = (TextView)convertView.findViewById(R.id.phonenumber);
			usernameTV.setText(entity.getUserName());
			TextView nicknameTV = (TextView)convertView.findViewById(R.id.user_name);
			nicknameTV.setText(entity.getNickName());
			ImageView imgView = (ImageView)convertView.findViewById(R.id.user_icon);
			imgView.setImageResource(R.drawable.family_member_add);


//			convertView.setTag(entity.getAccountName());
//			downloadHeadImage(entity.getAccountName(), convertView);
//			DownImageAsytask tasAsytask = new DownImageAsytask();
//			tasAsytask.execute(entity.getAccountName());

			String path = GPFileManager.getFileManager().createFilePath( "family");
			path +=  File.separator + entity.getAccountName() + ".jpg";
			bitmapLoaders.displayImage(imgView, GPLoginUserModel.get().getSessionId(), entity.getAccountName(), path);
			ImageView noteImageView = (ImageView)convertView.findViewById(R.id.login_note_icon);
			if (entity.getAccountName().equals(GPLoginUserModel.get().getAccountName()))
			{
				noteImageView.setVisibility(View.VISIBLE);
				noteImageView.setBackgroundResource(R.drawable.check);
			}
			else {
				noteImageView.setVisibility(View.GONE);
				
			}
			
		}
		
		
		
		return convertView;
	}
	
	@SuppressLint("NewApi") private void downloadHeadImage(final String accountName,final View view)
	{
		Log.d("DownImageAsytask", accountName);
		new GPRequestService().download(GPLoginUserModel.get().getSessionId(), accountName, new GPClientCallBack(){
			@Override
			public void onSuccess(String result) throws Exception {
				// TODO Auto-generated method stub
				super.onSuccess(result);
				Map<String, Object> map = GPUtils.getMapForJson(result);
//				int code = Integer.parseInt(map.get("code").toString());
				Log.d("DownImageAsytask", map.toString());
//				if (code == 200)
//				{
					Log.d("DownImageAsytask", accountName);
					JSONObject jsonObject = (JSONObject) map.get("result");
					String baseString = jsonObject.getString("base64");
				    ImageView imgView = 	(ImageView)view.findViewById(R.id.user_icon);
				   
				   
				   Bitmap bitmap =  GPBitmapUtils.stringtoBitmap(baseString);
				   String path = "family" + File.separator + accountName;
//				   imgView.setImageBitmap(bitmap);
				   GPBitmapUtils.saveImgToDirectory(bitmap, path);
				
				
					Message msg  = new Message();
					msg.obj = baseString;
					
//				}
				
			}
			
			@Override
			public void onError(Throwable ex) {
				// TODO Auto-generated method stub
				super.onError(ex);
				
				Log.d("DownImageAsytask", ex.getMessage());
				
			}
			
		});
	}
	
	class DownImageAsytask extends AsyncTask<String, Float, String>
	{
		private String baseString;
		
		@Override
		protected String doInBackground(String... params) {
			
			// TODO Auto-generated method stub
			Log.d("DownImageAsytask", params[0]);
			
			(new GPRequestService()).download(GPLoginUserModel.get().getSessionId(), params[0], new GPClientCallBack(){
				@Override
				public void onSuccess(String result) throws Exception {
					// TODO Auto-generated method stub
					super.onSuccess(result);
					Map<String, Object> map = GPUtils.getMapForJson(result);
//					int code = Integer.parseInt(map.get("code").toString());
//					if (code == 200)
					{
						JSONObject jsonObject = (JSONObject) map.get("result");
						baseString = jsonObject.getString("base64");
						
						Log.d("DownImageAsytask", map.toString());
					}
					
				}
				
				@Override
				public void onError(Throwable ex) {
					// TODO Auto-generated method stub
					super.onError(ex);
					
					Log.d("DownImageAsytask", ex.getMessage());
				}
			});
			
			return null;
		}
		
		
	}
	

}
