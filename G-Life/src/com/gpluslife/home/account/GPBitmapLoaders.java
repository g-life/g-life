package com.gpluslife.home.account;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;



import com.gpluslife.R;
import com.gpluslife.service.GPRequestService;
//import com.gpluslife.service.FileService;
//import com.gpluslife.service.http.ClientCallBack;
//import com.gpluslife.utils.Utils;
import com.gpluslife.service.http.GPClientCallBack;
import com.gpluslife.utils.GPBitmapUtils;
import com.gpluslife.utils.GPUtils;

public class GPBitmapLoaders {
	
	GPMemoryCache memoryCache=new GPMemoryCache();
//	public IputToFile fileCache;
	private Map<ImageView, String> imageViews=Collections.synchronizedMap(new WeakHashMap<ImageView, String>());
	ExecutorService executorService;
	Handler handler=new Handler();//handler to display images in UI thread
	
	
	  public GPBitmapLoaders(Context context){
//	        fileCache=new IputToFile(context);
	        executorService=Executors.newFixedThreadPool(10);
	    }
	
	  
	  
	  
	final int stub_id = R.drawable.ic_launcher;
	
	public void displayImage(final ImageView imageView, final String sesionid,final String accountName,final String imageURL){
		
		Log.d("DownImageAsytask ", imageURL);
		
		imageViews.put(imageView, imageURL);
		Bitmap bitmap = memoryCache.get(imageURL);

		if(bitmap != null){
			imageView.setImageBitmap(bitmap);
		}
		else{
			queuePhoto(sesionid,accountName, imageURL,imageView);
			imageView.setImageResource(stub_id);
		}
			
	}
	
	public Bitmap getSmallFile(String imageURL){
		  Bitmap b = null;
		  //			b = fileCache.getFile(imageURL);
		b = GPBitmapUtils.getBitmapFromDirectory(imageURL);
		return b;
		
	}

	/*
	 * 启动线程下载
	 */
	
	private void queuePhoto(String sesionid, String accountName,String imageURL, ImageView imageView) {
		// TODO 自动生成的方法存根
		PhotoToLoad p = new PhotoToLoad(sesionid,accountName,imageURL,imageView);
		executorService.submit(new PhotosLoader(p));
	}
	
	
	Bitmap bmp = null;
    private Bitmap getBitmap( final String sesionid,final String accountName,final String imageURL) {
		// TODO 自动生成的方法存根

        //from SD cache
        Bitmap b = null;
		b = GPBitmapUtils.getBitmapFromDirectory(imageURL);
		if (b != null)
			return b;
        
    	
//    	try{
    		
    		Log.d("DownImageAsytask"," account name ===>" + accountName);
    		
        new GPRequestService().download(sesionid, accountName, new GPClientCallBack(){
  				@Override
  				public void onSuccess(String jsonString)
  						throws Exception {
  					Map<String, Object> map = GPUtils.getMapForJson(jsonString);
  					
  					Log.d("DownImageAsytask", map.toString());
  						/*
  						 * 创建文件夹
  						 */
  						  						
  						JSONObject jsonObject = (JSONObject) map.get("result");
						String baseString = jsonObject.getString("base64");
  						Bitmap bitmap = GPBitmapUtils.stringtoBitmap(baseString);
						GPBitmapUtils.saveImgToDirectory(bitmap, imageURL);
						bmp = GPBitmapUtils.getBitmapFromDirectory(imageURL);
//						 bmp = bitmap ;
//  					}
  				}
  				
  				@Override
  				public void onError(Throwable ex) {
  				// TODO Auto-generated method stub
  					super.onError(ex);
  					Log.d("DownImageAsytask"," error ===>" +ex.getMessage());
  				}
  				
  			});
        return bmp;
//  }catch (Throwable ex){
//            ex.printStackTrace();
//            if(ex instanceof OutOfMemoryError)
//                memoryCache.clear();
//            return null;
//         }
    	
	
	}
	
	
	public class PhotoToLoad
	{
		public String imageURL;
        public ImageView imageView;
        public String sesionid;
        public String logid;
        public PhotoToLoad(String s, String l,  String u, ImageView i){
        	sesionid =s;
        	logid = l;
        	imageURL=u; 
            imageView=i;
        }
	}
	
	
	
    class PhotosLoader implements Runnable {
        PhotoToLoad photoToLoad;
        PhotosLoader(PhotoToLoad photoToLoad){
            this.photoToLoad=photoToLoad;
        }
        
        @Override
        public void run() {
            try{
                if(imageViewReused(photoToLoad))
                    return;
                Bitmap bmp=getBitmap(photoToLoad.sesionid,photoToLoad.logid,photoToLoad.imageURL);
                memoryCache.put(photoToLoad.imageURL, bmp);
              
                if(imageViewReused(photoToLoad))
                    return;
                BitmapDisplayer bd=new BitmapDisplayer(bmp, photoToLoad);
                handler.post(bd);
            }catch(Throwable th){
                th.printStackTrace();
            }
        }
    }
    

    
	class BitmapDisplayer implements Runnable
	{
		Bitmap bitmap;
		PhotoToLoad photoToLoad;
		public BitmapDisplayer(Bitmap b, PhotoToLoad p)
		{
			bitmap=b;
			photoToLoad = p;
		}
		@Override
		public void run() {
			// TODO 自动生成的方法存根
			if(imageViewReused(photoToLoad))
				return;
		
			if(bitmap != null)
				
				photoToLoad.imageView.setImageBitmap(bitmap);
			else
				photoToLoad.imageView.setImageResource(stub_id);	
			}
	}
	
	
	

	public boolean imageViewReused(PhotoToLoad photoToLoad) {
		String tag = imageViews.get(photoToLoad.imageView);
		if(tag == null || !tag.equals(photoToLoad.imageURL))
			return true;
		return false;
	}
	
  
}
