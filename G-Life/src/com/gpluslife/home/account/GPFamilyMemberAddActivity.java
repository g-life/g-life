package com.gpluslife.home.account;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;

import com.gpluslife.R;
import com.gpluslife.customView.GPNavigationView;
import com.gpluslife.login.model.GPLoginUserModel;
import com.gpluslife.service.GPRequestService;
import com.gpluslife.service.http.GPClientCallBack;
import com.gpluslife.utils.GPConstants;
import com.gpluslife.utils.GPErrorStlye;
//import com.gpluslife.service.MoreUserLoginMsg;
//import com.gpluslife.service.UserService;
//import com.gpluslife.service.http.ClientCallBack;
//import com.gpluslife.utils.ErrorStlye;
//import com.gpluslife.utils.GPErrorStlye;
//import com.gpluslife.utils.Utils;
//import com.modle.LoginUserModel;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("NewApi") public class GPFamilyMemberAddActivity extends Activity implements OnClickListener {

	private LinearLayout  family1,family2,friend1,friend2,other1,other2;
	private GPNavigationView mNavigationView;
	private Button mBackButton;
	private Button mDoneButton;
	private EditText Phone_number_edit,pawd_edit;
	private String relation="家人";
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		family1=(LinearLayout) findViewById(R.id.family1);
		family2=(LinearLayout) findViewById(R.id.family2);
		friend1=(LinearLayout) findViewById(R.id.friend1);
		friend2=(LinearLayout) findViewById(R.id.friend2);
		other1=(LinearLayout) findViewById(R.id.other1);
		other2=(LinearLayout) findViewById(R.id.other2);
		
		mNavigationView = (GPNavigationView)findViewById(R.id.navigationView);
		mBackButton = mNavigationView.getBtn_left();
		mDoneButton = mNavigationView.getBtn_right();
		mBackButton.setOnClickListener(this);
		mDoneButton.setOnClickListener(this);
		
		Phone_number_edit=(EditText) findViewById(R.id.Phone_number_edit);
		pawd_edit=(EditText) findViewById(R.id.pawd_edit);
		family1.setOnClickListener(this);
		family2.setOnClickListener(this);
		friend1.setOnClickListener(this);
		friend2.setOnClickListener(this);
		other1.setOnClickListener(this);
		other2.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.family1:
			relation="家人";
			family1.setVisibility(View.GONE);//bai
			family2.setVisibility(View.VISIBLE);//lan
			friend1.setVisibility(View.VISIBLE);//bai
			friend2.setVisibility(View.GONE);//lan
			other1.setVisibility(View.VISIBLE);//bai
			other2.setVisibility(View.GONE);//lan
			Log.d("selectionfamily", relation);
			return;
		case R.id.friend1:
			relation="朋友";
			friend1.setVisibility(View.GONE);//bai
			friend2.setVisibility(View.VISIBLE);//lan
			family1.setVisibility(View.VISIBLE);
			family2.setVisibility(View.GONE);
			other1.setVisibility(View.VISIBLE);//bai
			other2.setVisibility(View.GONE);//lan
			Log.d("selectionfriend", relation);
			return;
		case R.id.other1:
			relation="其他";
			other1.setVisibility(View.GONE);//bai
			other2.setVisibility(View.VISIBLE);//lan
			friend1.setVisibility(View.VISIBLE);//bai
			friend2.setVisibility(View.GONE);//lan
			family1.setVisibility(View.VISIBLE);
			family2.setVisibility(View.GONE);
			Log.d("selectionother", relation);
			return;
		case GPConstants.NAVIGATION_LEFT_BUTTON:
		{
			onBackPressed();
		}
			break;
		case GPConstants.NAVIGATION_RIGHT_BUTTON:
			try {
				submitToServer();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		default:
			break;
		}
	}
	public void  submitToServer() throws Exception{
		String phone = Phone_number_edit.getText().toString();
		String password=pawd_edit.getText().toString();
		String check = "^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$";
		Pattern p = Pattern.compile(check);
		Matcher m = p.matcher(phone);
		if (!m.matches()) {
			Toast.makeText(this, "手机格式错误！", Toast.LENGTH_SHORT).show();
			return;
		}
		if(phone.length()==0 || password.length()==0){
			Toast.makeText(this, "请输入帐号或密码！", Toast.LENGTH_SHORT).show();
		}
		if(phone.length()>0  && password.length()>0){
			//
			
			GPRequestService.get().addfamily(GPLoginUserModel.get().getSessionId(), Long.parseLong(phone), password, relation, new GPClientCallBack(){
				@Override
				public void onSuccess(String jsonString) throws Exception {
					super.onSuccess(jsonString);
					JSONObject jsonObj = new JSONObject(jsonString);
					int code = jsonObj.getInt("code");
					Log.d("code",code+" ");
					if(code ==200){
						Looper.prepare();
						Toast.makeText(GPFamilyMemberAddActivity.this, "添加成功！", Toast.LENGTH_SHORT).show();
						Intent intent=new Intent().setClass(GPFamilyMemberAddActivity.this, GPFamilyMemberActivity.class);
						startActivity(intent);
						GPFamilyMemberAddActivity.this.finish();
						Looper.loop();
					}
					GPErrorStlye errorStlye=new GPErrorStlye();
					errorStlye.stlye(GPFamilyMemberAddActivity.this, code);
				}
			});
		}
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}
	
}
