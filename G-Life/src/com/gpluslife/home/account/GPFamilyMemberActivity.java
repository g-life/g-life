package com.gpluslife.home.account;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.R.integer;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gpluslife.R;
import com.gpluslife.customView.GPFamilyMemberSlideView.OnSlideListener;
import com.gpluslife.customView.GPNavigationView;
import com.gpluslife.customView.ListViewCompats;
import com.gpluslife.home.account.adapter.GPFamilyMemberAdapter;
import com.gpluslife.home.account.model.GPFamilyMemberEntity;
import com.gpluslife.login.GPLoginActivity;
import com.gpluslife.login.GPLoginLoadingDialog;
//import com.gpluslife.service.MoreUserLoginMsg;
//import com.gpluslife.service.UserService;
//import com.gpluslife.service.http.ClientCallBack;
//import com.gpluslife.utils.ErrorStlye;
//import com.gpluslife.view.Family_member_SlideView;
//import com.gpluslife.view.Family_member_SlideView.OnSlideListener;
//import com.gpluslife.view.ListViewCompats;
//import com.modle.LoginUserModel;
import com.gpluslife.login.model.GPLoginUserModel;
import com.gpluslife.login.model.GPUserModelManager;
import com.gpluslife.service.GPRequestService;
import com.gpluslife.service.http.GPClientCallBack;
import com.gpluslife.utils.GPConstants;
import com.gpluslife.utils.GPErrorStlye;
import com.gpluslife.utils.GPUtils;

@SuppressLint("NewApi") public class GPFamilyMemberActivity extends Activity implements OnClickListener,OnSlideListener, OnItemClickListener {


	private static final String TAG = "GPFamilyMemberActivity";

	private GPNavigationView mNavigationView;
	private Button mBackButton;
	
	private  GPFamilyMemberAdapter adapter;
    private List<GPFamilyMemberEntity> memberList = new ArrayList<GPFamilyMemberEntity>();
//    private Family_member_SlideView mLastSlideViewWithStatusOn;
	private ListViewCompats familyListView;
	JSONArray result;
	LinearLayout  add_family_member;
	MyThread  myThread;
	
//	private Handler mHandler;
	
	@SuppressLint("WorldReadableFiles")
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_family_member);
		
		GPLoginUserModel userModel = GPLoginUserModel.get();
		GPFamilyMemberEntity entity = new GPFamilyMemberEntity();
		entity.setAccountName(userModel.getAccountName());
		entity.setNickName(userModel.getNickname());
		entity.setUserName(userModel.getUserName());
		memberList.add(entity);
		
		familyListView=(ListViewCompats) findViewById(R.id.list);
		familyListView.setOnItemClickListener(this);
		adapter = new GPFamilyMemberAdapter(this);
		familyListView.setAdapter(adapter);
		adapter.setMemberEntities(memberList);
		
		myThread=new MyThread();
		myThread.run();
		
		
		mNavigationView = (GPNavigationView)findViewById(R.id.navigationView);
		mBackButton = mNavigationView.getBtn_left();
		mBackButton.setOnClickListener(this);
		
	}
	class MyThread implements Runnable{
		   public void run(){
			   getfamilymember();
			   Log.d("dowload", "下载完毕sendMessage");
			  
				try {
					Thread.sleep(300);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		   }
	}
	Handler  mHandler=new Handler(){
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			// 视图更新。
			
			if (msg.what == 1)
			{
				adapter.notifyDataSetChanged();  
//				adapter.notifyDataSetChanged();  
//				Log.d("adapter", "刷新adapter");
			}
			else if (msg.what == 2)
			{
//				onBackPressed();
				
				 Intent data=new Intent();  
//		            data.putExtra("bookname", str_bookname);  
//		            data.putExtra("booksale", str_booksale);  
		            //请求代码可以自己设置，这里设置成20  
		            setResult(200, data);  
				finish();
			}
			
			
//			family_list.setAdapter(adapter);
		}
	};
	public void getfamilymember(){
		GPLoginUserModel userModel = GPLoginUserModel.get();
		GPRequestService.get().getfamily(userModel.getSessionId(), new GPClientCallBack(){
			@Override
			public void onSuccess(String jsonString) throws Exception {
				super.onSuccess(jsonString);
				Map<String, Object> map = GPUtils.getMapForJson(jsonString);
				Log.d(TAG, "map == > " + map.toString());
				int code = Integer.parseInt(map.get("code").toString());
				Log.d("code",code+" ");
				if(code ==200){
					 @SuppressWarnings("unused")
					JSONArray jsonArray = (JSONArray) map.get("result");
					int size = jsonArray.length();
					  for( int i = 0; i < size; i++)
					  {
						  JSONObject jsonObject = jsonArray.getJSONObject(i);
						  Log.d(TAG, "obj == > " + jsonObject.toString());
						  GPFamilyMemberEntity entity = new GPFamilyMemberEntity();
//						  String userName = jsonObject.getString("phone");
						  entity.setUserName( jsonObject.getString("phone"));
						  entity.setAccountName( jsonObject.getString("accountName"));
						  entity.setNickName( jsonObject.getString("nickname"));
						  entity.setRelationShip( jsonObject.getString("relation"));
						  entity.setSha1PWD(jsonObject.getString("sha1Password"));
						  memberList.add(0, entity);
					  }
					  Message msg = new Message();
					  msg.what = 1;
					  mHandler.sendMessage(msg);
				}
				else {
					GPErrorStlye errorStlye=new GPErrorStlye();
					errorStlye.stlye(GPFamilyMemberActivity.this, code);
				}
				
				
			}
		});
	}

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
            long id) {
        TextView ctxtView =(TextView) view.findViewById(R.id.phonenumber);
        String phonenumber=ctxtView.getText().toString();
        Log.e(TAG, "onItemClick position=" + position+"phonenumber="+phonenumber.toString());
//        CheckLogin getUserInfo =new CheckLogin();
//        LoginLoadingDialog loadingDialog=new LoginLoadingDialog(this);
//        getUserInfo.Userlogin(phonenumber.toString(), mMessageItems.get(position).password, this, loadingDialog);
        
        if(position == memberList.size())
        {
        		Intent intent = new Intent(GPFamilyMemberActivity.this,GPFamilyMemberAddActivity.class);
        		startActivity(intent);
        }
        else {
	        	GPFamilyMemberEntity entity = memberList.get(position);	
	        	if (!entity.getAccountName().equals(GPLoginUserModel.get().getAccountName()))
	        	{
	            	String deviceKey = Secure.getString(
							GPFamilyMemberActivity.this.getContentResolver(),
							Secure.ANDROID_ID);
	            	final String username = entity.getUserName();
	            	final String password = entity.getSha1PWD();
	            	final GPLoginLoadingDialog loadingDialog = new GPLoginLoadingDialog(GPFamilyMemberActivity.this);
					loadingDialog.show();
		        	GPRequestService.get().switchAccountLogin(username, password, deviceKey, new GPClientCallBack()
		        	{
						@Override
						public void onSuccess(String result) throws Exception {
							// TODO Auto-generated method stub
							super.onSuccess(result);
							Log.d(TAG, "login success! result => " + result);
							Map<String, Object> map = GPUtils
									.getMapForJson(result);
							
							int code = 0;
							if (map.containsKey("code")) 
							{
								code = Integer.parseInt(map.get("code")
										.toString());
							}
							if (code == 200)
							{
								if (map.containsKey("result")) 
								{
									String resultString = map.get("result")
											.toString();
//									GPUserModelManager.destroyUserModel(GPFamilyMemberActivity.this);
									GPLoginUserModel userModel = GPUserModelManager.parserJsonString(GPFamilyMemberActivity.this, resultString,username,false);
//									GPUserModelManager.saveUserModelToSharePreferences(GPFamilyMemberActivity.this, userModel, password);
									GPUserModelManager.saveTempUserModelToSharePreferences(GPFamilyMemberActivity.this, userModel, password);
									loadingDialog.dismiss();
									
									Message msg = new Message();
									  msg.what = 2;
									  mHandler.sendMessage(msg);
									Looper.prepare();
									Toast.makeText(GPFamilyMemberActivity.this, "切换成功！", Toast.LENGTH_SHORT).show();
									Looper.loop();
									
									
									
								}
							}
							else {
								GPErrorStlye errorStlye=new GPErrorStlye();
								errorStlye.stlye(GPFamilyMemberActivity.this, code);
							}
							
						}

						@Override
						public void onError(Throwable ex) {
							// TODO Auto-generated method stub
							super.onError(ex);
							
							loadingDialog.dismiss();
						}
		        	});
			}
	
		}
    }

    @Override
    public void onSlide(View view, int status) {
//        if (mLastSlideViewWithStatusOn != null && mLastSlideViewWithStatusOn != view) {
//            mLastSlideViewWithStatusOn.shrink();
//        }
//
//        if (status == SLIDE_STATUS_ON) {
//            mLastSlideViewWithStatusOn = (Family_member_SlideView) view;
//        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//		case R.id.add_family_member:
//		{
//			Intent intent=new Intent().setClass(this, GPFamilyMemberAddActivity.class);
//			startActivity(intent);
//		}
//			 break;
		case R.id.holder2:
		{
			 delete();
		}
			 break;
		case GPConstants.NAVIGATION_LEFT_BUTTON:
		{
			onBackPressed();
			 finish();
		}
			 break;
		default:
			break;
		}
    }
    public void delete(){
    	
//    	GPRequestService.get().deletefamily(sessionId, accountName, new ClientCallBack(){
//    		@Override
//    		public void onSuccess(String jsonString) throws Exception {
//    			super.onSuccess(jsonString);
//    			JSONObject jsonObj = new JSONObject(jsonString);
//				int code = jsonObj.getInt("code");
//				Log.d("code",code+" ");
//				ErrorStlye errorStlye=new ErrorStlye();
//				errorStlye.stlye(GPFamilyMemberActivity.this, code);
//				if(code ==200){
//					Looper.prepare();
//					Toast.makeText(GPFamilyMemberActivity.this, "删除成功！", Toast.LENGTH_SHORT).show();
//					myThread=new MyThread();
//					myThread.run();
//					Looper.loop();
//				}
//    		}
//    	});
    }
    
    @Override
	protected void onResume() {
		super.onResume();
//		MoreUserLoginMsg moreUserLoginMsg =new MoreUserLoginMsg(this);
//		moreUserLoginMsg.Dialog();
	}

}
