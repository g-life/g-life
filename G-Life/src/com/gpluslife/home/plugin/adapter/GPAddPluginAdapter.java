package com.gpluslife.home.plugin.adapter;

import java.util.ArrayList;
import java.util.zip.Inflater;

import com.gpluslife.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


public class GPAddPluginAdapter extends BaseAdapter {
	
	private ArrayList<String> pluginList  ;
	private Activity context;
	
	public GPAddPluginAdapter(Activity ctx)
	{
		context = ctx;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-gen erated method stub
		int count = pluginList.size();
		return count;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		Object obj = pluginList.get(position);
		return obj;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@SuppressLint("ViewHolder") @Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		convertView=View.inflate(context, R.layout.add_device_item, null);
		TextView pluginTextView = (TextView)convertView.findViewById(R.id.puls_name);
		
		String pluginName = (String) getItem(position);
		pluginTextView.setText(pluginName);
		
	 
		
		return convertView;
	}

	public void setPluginList(ArrayList<String> pluginList) {
		this.pluginList = pluginList;
	}

	
	
}
