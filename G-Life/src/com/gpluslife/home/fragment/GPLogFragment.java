package com.gpluslife.home.fragment;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

//import notes.DelectOrShare;
//import notes.GetServiceToDb;
//import notes.Getdbfile;
//import notes.LogAdapterNotes;
//import notes.ModleTextandView;
//import pulltorefresh.PullToRefreshView;
//import pulltorefresh.PullToRefreshView.OnFooterRefreshListener;
//import pulltorefresh.PullToRefreshView.OnHeaderRefreshListener;
import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemLongClickListener;


import com.gpluslife.R;
import com.gpluslife.filemanager.GPFileManager;


public class GPLogFragment extends Fragment{
	
	public GPLogFragment(){}
	
	private String TAG="Log_Fragment";
	private String sessionId = null;
	private String phone = null;
	private ListView log_list;
//	private Getdbfile getdbfile;
//	public GetServiceToDb getServicedb;
	public SQLiteDatabase sqlitedb;
	private int page = 0;
	private int OldListY = -1;
//	public LogAdapterNotes logAdapter;
//	PullToRefreshView mPullToRefreshView;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_log, container, false);
//        SharedPreferences sp = getActivity().getSharedPreferences("sessionId",getActivity().MODE_WORLD_READABLE);
//		sessionId = sp.getString("sessionId", sessionId);
//		phone = sp.getString("username", phone);
        
//    	log_list = (ListView) rootView.findViewById(R.id.log_list);
//    	log_list.setOnItemLongClickListener(new ListViewHandle());

//    	getServicedb = new GetServiceToDb(getActivity());
//		getServicedb.WriteToDb(sessionId);
//		getdbfile = new Getdbfile(getActivity());

	
		String phone = null;
		
//		phone =sp.getString("username", phone);
//		
//		File DBFilew = new File("/mnt/sdcard/Gplus/"+phone+"/"+"Gpuls.db");
//		sqlitedb = SQLiteDatabase.openOrCreateDatabase(DBFilew, null);
//		sqlitedb.execSQL(DBHelper.sql);
		
//		SendDataToAdapter(page);
        return rootView;
    }
	/*
	 * item长按事件
	 */
	/*
	private class ListViewHandle implements OnItemLongClickListener {

		@Override
		public boolean onItemLongClick(final AdapterView<?> groupView,
				final View view, int arg2, long arg3) {
//			final ModleTextandView ImageAndText;
//			final List<ModleTextandView> dataAarrays = new ArrayList<ModleTextandView>();
//			ImageAndText = logAdapter.getItem(arg2);
//			dataAarrays.add(ImageAndText);
			int[] location = { -1, -1 };
			view.getLocationOnScreen(location);
			OldListY = location[1];
//			final DelectOrShare selectDialog = new DelectOrShare(getActivity(),
//					R.style.dialog);// 创建Dialog并设置样式主题
//			Window win = selectDialog.getWindow();
			LayoutParams params = new LayoutParams();
			params.y = OldListY - 1000;// 设置y坐标
//			win.setAttributes(params);
//			selectDialog.setCanceledOnTouchOutside(true);// 设置点击Dialog外部任意区域关闭Dialog
//			selectDialog.show();

//			TextView delete = (TextView) selectDialog.findViewById(R.id.yichu);
//			TextView share = (TextView) selectDialog.findViewById(R.id.gongxiang);
			delete.setOnClickListener(new OnClickListener() {
				@SuppressLint("ShowToast")
				@Override
				public void onClick(View arg0) {
					FileService.get().deletenote(sessionId,
							ImageAndText.getLOGID(), new ClientCallBack() {

								@SuppressLint("NewApi")
								public void onSuccess(String jsonString)
										throws Exception {
									System.out.println("查看服务器的删除的JSON数据："+ jsonString);
									JSONObject jsonObj = new JSONObject(jsonString);
									int code = jsonObj.getInt("code");
									if (code == 200) {

										String where = ImageAndText.getLOGID();
										System.out.println("查看服务where：" + where);
										
										  判断获取到的数据是否已经存在数据库
										 
										String selection = "logid =? ";
										String[] selections = new String[] { where };
										int id = sqlitedb.delete(DBHelper.TABLE_NAME,selection, selections);
										System.out.println("查看服务器的删除的数据：" + id);
										Cursor c =sqlitedb.query(DBHelper.TABLE_NAME,null, null, null, null, null, null);
										System.out.println("移除后数据库还剩的数据数目"+ c.getCount());
										logAdapter.removeNewsItem(dataAarrays);
										logAdapter.notifyDataSetChanged();
									} else if (code == 102001) {
										Log.d(TAG, "No data !");
										Looper.prepare();
										Toast.makeText(getActivity(), "没有数据!",500).show();
										Looper.loop();
									} else {
										Looper.prepare();
										Toast.makeText(getActivity(), "获取数据失败",500).show();
										Looper.loop();
									}
								}
							});
					selectDialog.cancel();
				}
			});
			share.setOnClickListener(new OnClickListener() {
				@SuppressLint("ShowToast")
				@Override
				public void onClick(View arg0) {
					showShare(view);
					selectDialog.cancel();
				}
			});
			return true;
		}
	}
	*/
	/**
	 * 启动一键分享
	 */
	/*
	private void showShare(View view) {
        ShareSDK.initSDK(getActivity());
        OnekeyShare oks = new OnekeyShare();
        //关闭sso授权
        oks.disableSSOWhenAuthorize();
        
        // 分享时Notification的图标和文字
        //oks.setNotification(R.drawable.ic_launcher, getString(R.string.app_name));
        // title标题，印象笔记、邮箱、信息、微信、人人网和QQ空间使用
        oks.setTitle(getString(R.string.sharesdk));
        // titleUrl是标题的网络链接，仅在人人网和QQ空间使用
        oks.setTitleUrl("http://sharesdk.cn");
        // text是分享文本，所有平台都需要这个字段
        oks.setText("Gplus");
        // imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
        oks.setImagePath(saveImage(view));
        //oks.setImageUrl("http://99touxiang.com/public/upload/nvsheng/125/27-011820_433.jpg");

        // url仅在微信（包括好友和朋友圈）中使用
        oks.setUrl("https://itunes.apple.com/cn/app/bao-wei-luo-bo/id534453594?mt=8");
        // comment是我对这条分享的评论，仅在人人网和QQ空间使用
        oks.setComment("我是测试评论文本");
        // site是分享此内容的网站名称，仅在QQ空间使用
        oks.setSite(getString(R.string.app_name));
        // siteUrl是分享此内容的网站地址，仅在QQ空间使用
        oks.setSiteUrl("http://www.csdn.net/");
        oks.setDialogMode();
        
        // 启动分享GUI
        oks.show(getActivity());
   }
   */
	/**
	 * 保存图片到本地
	 * @param view
	 * @return
	 */
	private String saveImage(View view){
		String filePath = null;
		String dirPath = getSDCardPath();
		if (null == dirPath) {
			dirPath = "/mnt/sdcard/Gplus/"+phone+File.separator + "SharedImages" + File.separator;
		} else {
			dirPath = dirPath + File.separator + "Gplus" + File.separator + phone + File.separator + "SharedImages" + File.separator;
		}
		try {  
            File path = new File(dirPath);  
            if(!path.exists()){  
            	path.mkdirs();  
            }  
            //文件  
            filePath = dirPath + System.currentTimeMillis() + ".png";  
            File file = new File(filePath);  
            if (!file.exists()) {  
                file.createNewFile();  
            }  

            FileOutputStream fos = null;  
            fos = new FileOutputStream(file);  
            if (null != fos) {
            	Bitmap bitMap = getSourceBitmap(view);
            	if(bitMap != null){
            		bitMap.compress(Bitmap.CompressFormat.PNG, 90, fos);  
            	}
                fos.flush();  
                fos.close();    
                //Toast.makeText(this, "截屏文件已保存至SDCard/AndyDemo/ScreenImage/下", Toast.LENGTH_LONG).show();  
            }  
        } catch (Exception e) {  
            e.printStackTrace();  
        }
		return filePath;
	}
	
	/**
     * 获取SDCard的目录路径功能
     * @return
     */
    private String getSDCardPath(){
        //判断SDCard是否存在
        boolean sdcardExist = Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
        if(sdcardExist){
            File sdcardDir = Environment.getExternalStorageDirectory();
            return sdcardDir.toString();
        }
        return null;
    }
	
	/**
	 * 根据view生成图片
	 * @param view
	 * @return
	 */
	private Bitmap getSourceBitmap(View view){
		Bitmap bp = null;
		if(view != null){
			view.setDrawingCacheEnabled(true);
			view.buildDrawingCache();
			Bitmap bt = view.getDrawingCache();
			bp = Bitmap.createBitmap(bt,0,0,view.getWidth(),view.getHeight());
			view.destroyDrawingCache();
		}
		return bp;
	}
	
	
	
	/*
	 * 启动异步线程传值到Adapter，加载数据
	 */

	public void SendDataToAdapter(final int page) {
		final Handler handler = new Handler() {
			@SuppressWarnings("unchecked")
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
//				logAdapter = new LogAdapterNotes(getActivity(),
//						(List<ModleTextandView>) msg.obj, sessionId);
//				log_list.setAdapter(logAdapter);

			}
		};

		new Thread() {
			public void run() {

//				Getdbfile getdbfile = new Getdbfile(getActivity());
//				List<ModleTextandView> dataAarrays = new ArrayList<ModleTextandView>();
//				dataAarrays = getdbfile.getModleId(page);
//				Message msg = new Message();
//				msg.obj = dataAarrays;
//				handler.sendMessage(msg);

			}
		}.start();
	}
	
}
