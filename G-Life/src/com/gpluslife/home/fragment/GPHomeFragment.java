package com.gpluslife.home.fragment;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
//import notes.DelectOrShare;
//import notes.GetServiceToDb;
//import notes.Getdbfile;
//import notes.LogAdapterNotes;
//import notes.ModleTextandView;

import org.json.JSONObject;

//import pulltorefresh.PullToRefreshView;
//import pulltorefresh.PullToRefreshView.OnFooterRefreshListener;
//import pulltorefresh.PullToRefreshView.OnHeaderRefreshListener;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gpluslife.R;
import com.gpluslife.home.plugin.GPAddPluginActivity;
//import com.gpluslife.activity.AddDevc;
//import com.gpluslife.activity.MainActivity;
//import com.gpluslife.http.ClientCallBack;
//import com.gpluslife.log.DBHelper;
//import com.gpluslife.service.FileService;
import com.gpluslife.utils.GPPluginManager;
import com.gpluslife.utils.GPGlobalConstance;
import com.gpluslife.utils.GPPreferencesUtils;
//import com.gpluslife.view.SegmentView;
//import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

@SuppressLint({ "SdCardPath", "WorldReadableFiles" })
public class GPHomeFragment extends Fragment implements OnClickListener,
		OnItemClickListener {
	
//	protected SlidingMenu mSlidingMenu;
	public static final String ITEM_NAME_WALKS = "walks";
	public static final String ITEM_NAME_DISTANCE = "distance";
	public static final String ITEM_NAME_CALORI = "calori";
	public static final String ITEM_NAME_STAIRS = "stairs";
	public static final String ITEM_NAME_SLEEP = "sleep";
	private static final String TAG = "Fragment_home";
	private RelativeLayout llayout;
	private ImageView ivTitleBtnRight1, ivTitleBtnRight2;
	private LinearLayout ivTitleBtnLeft;
	private ListView log_list;
	private LinearLayout home_container;
	private LinearLayout home, log;
	private LinearLayout transform_my;
//	private SegmentView mytest;
	private TextView deceName;
	private String sessionId = null;
	private Bitmap bitmap;
	private Drawable drawable;
	protected String pic;
	private String phone = null;
	private ImageView ivTitleBtnLeft2;
	private LinearLayout speed_add_plus;
	
	

	@SuppressWarnings({ "deprecation", "static-access" })
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_home, container, false);

//		SharedPreferences sp = getActivity().getSharedPreferences("sessionId",getActivity().MODE_WORLD_READABLE);
//		sessionId = sp.getString("sessionId", sessionId);
//		phone = sp.getString("username", phone);
//		
//		ivTitleBtnLeft = (LinearLayout) view.findViewById(R.id.ivTitleBtnLeft1);
//		ivTitleBtnLeft2 = (ImageView) view.findViewById(R.id.ivTitleBtnLeft2);
//		ivTitleBtnLeft.setOnClickListener(this);
//
//		ivTitleBtnRight1 = (ImageView) view.findViewById(R.id.ivTitleBtnRigh1);
//		ivTitleBtnRight1.setOnClickListener(this);
//		ivTitleBtnRight2 = (ImageView) view.findViewById(R.id.ivTitleBtnRigh2);
//		ivTitleBtnRight2.setOnClickListener(this);
		
		// ======================================================
//		filepathimg = filepath + "/" + "Gjname" + ".jpg";
//		File f = new File(filepathimg);
//		Log.d(TAG, "filepathimg构建成功");
//		if (f.exists()) {
//			Log.d(TAG, "存在文件");
//			bitmap = BitmapFactory.decodeFile(filepathimg);
//			drawable = new BitmapDrawable(bitmap);
//			ivTitleBtnLeft2.setBackgroundDrawable(drawable);
//			Log.d(TAG, "HOME设置头像成功");
//		}
		// ========================================================
		
//		mytest = (SegmentView) getActivity().findViewById(R.id.test2014);
//		mytest.setOnClickListener(this);
//		llayout = (RelativeLayout) view.findViewById(R.id.main_layout_overturn);
//
		home_container = (LinearLayout) view.findViewById(R.id.home_container);
//
//
//		//快速添加插件
		speed_add_plus = (LinearLayout) view.findViewById(R.id.speed_add_plus);
		speed_add_plus.setOnClickListener(this);
//
		deceName = (TextView) view.findViewById(R.id.dvc_name);

		return view;
	}

	
	// ======================黄金分割线=====================================
	private void initFunctionList(Context context) {
		home_container.removeAllViews();
		if (GPPreferencesUtils.getBoolean(context,GPGlobalConstance.GROBAL_PREFERENCE_KEY_DEVICE_BLACELET_ADDED,false))
		{
			GPPluginManager.addPluginViews(home_container,"home_blacelet_all",GPGlobalConstance.GROBAL_PREFERENCE_KEY_DEVICE_BLACELET_ADDED);
			speed_add_plus.setVisibility(View.GONE);
			home_container.invalidate();
		}
		
	}

	
	// ========================================================================

	@Override
	public void onClick(View v) {
		Fragment newContent = null;
		switch (v.getId()) {
		case 0://主页
			newContent = new GPHomeFragment();
			ivTitleBtnRight1.setVisibility(View.VISIBLE);
			ivTitleBtnRight2.setVisibility(View.GONE);
			break;
		case 1://log页面
			newContent = new GPLogFragment();
			ivTitleBtnRight1.setVisibility(View.GONE);
			ivTitleBtnRight2.setVisibility(View.VISIBLE);
			break;
		case R.id.speed_add_plus://快速添加插件
			Intent intent = new Intent().setClass(getActivity(), GPAddPluginActivity.class);
			startActivity(intent);
			
			break;

		default:
			break;
			
		}
		if (newContent != null) {
			switchFragment(newContent);
		}
	}

	private void switchFragment(Fragment fragment) {
		if (getActivity() == null)
			return;
//		((MainActivity) getActivity()).switchContent(fragment);
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// ((HomeBaseiView) view).onClicked();
	}

	@Override
	public void onDestroy() {
	
		super.onDestroy();


	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
		initFunctionList(getActivity());
	
	}

	public void updateDeviceStatue(String deviceStatue) {
		deceName.setText(deviceStatue);
	}

}
