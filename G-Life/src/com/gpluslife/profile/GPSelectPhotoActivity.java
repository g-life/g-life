package com.gpluslife.profile;


import com.gpluslife.R;
import com.gpluslife.utils.GPFileUtils;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;


public class GPSelectPhotoActivity extends Activity implements OnClickListener{

	private static final String TAG = "SelectPicActivity";
	public static final int TACK_PHOTO = 1;
	public static final int PICK_PHOTO = 2;
	public static final String KEY_PHOTO_PATH = "photo_path";
	private LinearLayout dialogLayout;
	private Button takePhotoBtn,pickPhotoBtn,cancelBtn;
	private String picPath;
	private Intent lastIntent ;
	private Uri photoUri;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select_picture_layout);
		initView();
	}
	/**
	 * ��ʼ������View
	 */
	private void initView() {
		dialogLayout = (LinearLayout) findViewById(R.id.dialog_layout);
		dialogLayout.setOnClickListener(this);
		takePhotoBtn = (Button) findViewById(R.id.btn_take_photo);
		takePhotoBtn.setOnClickListener(this);
		pickPhotoBtn = (Button) findViewById(R.id.btn_pick_photo);
		pickPhotoBtn.setOnClickListener(this);
		cancelBtn = (Button) findViewById(R.id.btn_cancel);
		cancelBtn.setOnClickListener(this);
		lastIntent = getIntent();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.dialog_layout:
			finish();
			break;
		case R.id.btn_take_photo:
			takePhoto();
			break;
		case R.id.btn_pick_photo:
			slectImageEvent();
			break;
		default:
			finish();
			break;
		}
	}

	private void takePhoto() {
		String SDState = Environment.getExternalStorageState();
		if(!SDState.equals(Environment.MEDIA_MOUNTED)){
		}
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);//"android.media.action.IMAGE_CAPTURE"
		ContentValues values = new ContentValues();  
		photoUri = this.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);  
		intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, photoUri);
		startActivityForResult(intent, TACK_PHOTO);  
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		finish();
		return super.onTouchEvent(event);
	}
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode != Activity.RESULT_OK){finish();return;};
		doPhoto(requestCode,data);
	}
	
	private void doPhoto(int requestCode,Intent data)
	{
		if(requestCode == PICK_PHOTO){
			if (null == data) {finish();return;}
			photoUri = data.getData();
		}
		picPath = GPFileUtils.getPath(getApplicationContext(), photoUri);
		if(picPath != null && ( picPath.endsWith(".png") || 
				picPath.endsWith(".PNG") 
				||picPath.endsWith(".jpg")
				||picPath.endsWith(".JPG") 
				||picPath.endsWith(".jpeg") 
				||picPath.endsWith(".JPEG") 
				))
		{
			lastIntent.putExtra(KEY_PHOTO_PATH, picPath);
			setResult(Activity.RESULT_OK, lastIntent);
			finish();
		}
		else{
		}
	}

	public void slectImageEvent() {
		Intent target = GPFileUtils.createGetImageIntent();
		Intent intent = Intent.createChooser(target, "请选择照片！");
		try {
			startActivityForResult(intent, PICK_PHOTO);
		} catch (ActivityNotFoundException e) {

		}
	}
}
