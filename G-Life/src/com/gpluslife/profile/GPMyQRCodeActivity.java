package com.gpluslife.profile;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Map;

import com.gpluslife.R;
import com.gpluslife.customView.GPNavigationView;
import com.gpluslife.login.model.GPLoginUserModel;
import com.gpluslife.service.GPRequestService;
import com.gpluslife.service.http.GPClientCallBack;
import com.gpluslife.utils.GPBitmapUtils;
import com.gpluslife.utils.GPConstants;
import com.gpluslife.utils.GPUtils;
//import com.gpluslife.service.MoreUserLoginMsg;

import android.R.drawable;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

@SuppressLint({ "SdCardPath", "NewApi" })
public class GPMyQRCodeActivity extends Activity implements OnClickListener{

	private static final String TAG = "MyQr_code";
	private ImageView qr_img;
	private String filepath = "/mnt/sdcard/Gplus", filepathimg = "";
	
	private GPNavigationView mNavigationView;
	private Button mBackButton;
	private Button mSaveButton;
	
	private Handler mHandler;
	
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_qrcode);
		
		mHandler = new Handler(){
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				super.handleMessage(msg);
				Bundle bundle = msg.getData();
				byte[] qrcodeByte = bundle.getByteArray("qrcodeBitmap");
				Bitmap qrcodeBitmap = BitmapFactory.decodeByteArray(qrcodeByte, 0, qrcodeByte.length);
				qr_img.setImageBitmap(qrcodeBitmap);
				qr_img.invalidate();
			}
		};
		
		downloadQRCode();
		qr_img = (ImageView)findViewById(R.id.qr_img);
		mNavigationView = (GPNavigationView)findViewById(R.id.navigationView);
		mBackButton = mNavigationView.getBtn_left();
		mBackButton.setOnClickListener(this);
		mSaveButton = mNavigationView.getBtn_right();
		mSaveButton.setOnClickListener(this);
	
		// ======================================================
//				filepathimg ="/mnt/sdcard/Gplus/"+phone+  "/" + "Gjqr" + ".jpg";
//				File f = new File(filepathimg);
//				Log.d(TAG, "filepathimg构建成功");
//				if (f.exists()) {
//					Log.d(TAG, "存在文件");
//					Bitmap bitmap = BitmapFactory.decodeFile(filepathimg);
//					Drawable drawable = new BitmapDrawable(bitmap);
//					qr_img.setBackgroundDrawable(drawable);
//					Log.d(TAG, "个人信息设置设置头像成功");
//				}

				// ========================================================
		
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case GPConstants.NAVIGATION_LEFT_BUTTON:
		{
			onBackPressed();
		}
			finish();
			break;
		case GPConstants.NAVIGATION_RIGHT_BUTTON:
//			showsave();
			Toast.makeText(this, "保存二维码名片按钮！", 2000).show();
		break;
		default:
			break;
		}
	}
	
	public void downloadQRCode()
	{
		GPLoginUserModel userModel = GPLoginUserModel.get();
		GPRequestService.get().downloadqrcode(userModel.getSessionId(), userModel.getAccountName(),new GPClientCallBack(){
			
			@Override
			public void onSuccess(String result) throws Exception {
				// TODO Auto-generated method stub
				super.onSuccess(result);
				
				Map<String, Object> resultMap = GPUtils.getMapForJson(result);
				int code = 0;
				if (resultMap.containsKey("code"))
				{
					code = Integer.parseInt(resultMap.get("code").toString());
				}
				if (code == 200)
				{
					String resultString = resultMap.get("result").toString();
					Map<String, 	Object> map = GPUtils.getMapForJson(resultString);
					String base64sString = map.get("base64").toString();
					Bitmap headBitmap = GPBitmapUtils.stringtoBitmap(base64sString);
					Bitmap qrcodeBitmap= GPBitmapUtils.toRoundBitmap(headBitmap);
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					qrcodeBitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
					byte[] qrcodeByte = baos.toByteArray();
					
					Bundle bundle = new Bundle();
					bundle.putByteArray("qrcodeBitmap", qrcodeByte);
					Message msg = new Message();
					msg.setData(bundle);
					mHandler.sendMessage(msg);
				}
			}
			
		});
	}
	
	private void showsave() {
		// TODO Auto-generated method stub
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
//		MoreUserLoginMsg moreUserLoginMsg =new MoreUserLoginMsg(this);
//		moreUserLoginMsg.Dialog();
	}
}
