package com.gpluslife.profile;

import java.io.File;
import java.util.Calendar;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.gpluslife.R;
import com.gpluslife.customView.GPNavigationView;
import com.gpluslife.entity.GPBaseHttpEnitity;
import com.gpluslife.filemanager.GPExternalStorage;
import com.gpluslife.filemanager.GPFileManager;
import com.gpluslife.home.GPHomeActivity;
import com.gpluslife.login.GPLoginActivity;
import com.gpluslife.login.model.GPLoginUserModel;
import com.gpluslife.login.model.GPUserModelManager;
//import com.gpluslife.http.ClientCallBack;
//import com.gpluslife.service.FileService;
//import com.gpluslife.service.MoreUserLoginMsg;
import com.gpluslife.service.GPRequestService;
import com.gpluslife.service.http.GPClientCallBack;
import com.gpluslife.utils.GPBitmapUtils;
import com.gpluslife.utils.GPConstants;
//import com.gpluslife.utils.BitmapUtils;
import com.gpluslife.utils.GPErrorStlye;
import com.gpluslife.utils.GPFileUtils;
import com.gpluslife.utils.GPUtils;

@SuppressLint("NewApi")
public class GPUserProfileActivity extends Activity implements OnClickListener {

	protected static final int TO_UPLOAD_FILE = 1;
	protected static final int UPLOAD_FILE_DONE = 2;
	public static final int TO_SELECT_PHOTO = 3;

	private static final int REQUEST_CAPTURE_IMAGE = 4;
	private static final int REQUEST_SELECT_IMAGE = 5;

	private EditText mEt_userNick, mEt_userqm;
	private TextView district, phonenum;
	private ImageView imageView;
	private Button mBtn_birth, checkout_btn;
	private String picPath = null;
	private String TAG = "ImageFragment";

	private int district1 = 101010100;
	static PopupWindow popupWindow;
	private LinearLayout QR_code, linearLayout1;
	private TextView test;
	boolean state = true;
	private int gender;
	AnsyTry anys = null;
	Bitmap bitmap = null;
	String icon;

	private GPNavigationView navigationView;
	private Button mBackButton;
	private Button mDoneButton;

	private File mImageCaptured;

	private GPLoginUserModel userModel;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_profile);

		initView();

	}

	private void initView() {
		userModel = GPLoginUserModel.get();

		// =========================
		test = (TextView) findViewById(R.id.test);
		if (userModel.getGender() == 0) {
			test.setText("男");
		} else {
			test.setText("女");
		}
		test.setOnClickListener(this);

		// ==============================
		phonenum = (TextView) findViewById(R.id.phonenumber);
		phonenum.setText(userModel.getUserName());
		imageView = (ImageView) findViewById(R.id.id_avart_image);
		
		String iconPathString =GPFileManager.getFileManager().createFilePath(null);
		iconPathString += File.separator + "HEADICON";
		Bitmap bitmap = GPBitmapUtils.getBitmapFromDirectory(iconPathString);
		imageView.setImageBitmap(bitmap);
		bitmap = null;

		imageView.setOnClickListener(this);
		linearLayout1 = (LinearLayout) findViewById(R.id.linearLayout1);
		linearLayout1.setOnClickListener(this);

		// ========================================================
		mEt_userNick = (EditText) findViewById(R.id.set_user_nick);
		mEt_userNick.setText(userModel.getNickname());
		mEt_userqm = (EditText) findViewById(R.id.set_user_qm);
		mEt_userqm.setText(userModel.getSignature());
		mBtn_birth = (Button) findViewById(R.id.set_user_birthday);
		mBtn_birth = (Button) findViewById(R.id.set_user_birthday);
		mBtn_birth.setText(userModel.getBirthday());
		mBtn_birth.setOnClickListener(this);
		checkout_btn = (Button) findViewById(R.id.checkout_btn);
		checkout_btn.setOnClickListener(this);

		QR_code = (LinearLayout) findViewById(R.id.mask);
		QR_code.setOnClickListener(this);
		district = (TextView) findViewById(R.id.set_district);
		district.setOnClickListener(this);

		navigationView = (GPNavigationView) findViewById(R.id.navigationView);
		mBackButton = navigationView.getBtn_left();
		mBackButton.setOnClickListener(this);
		mDoneButton = navigationView.getBtn_right();
		mDoneButton.setOnClickListener(this);
	}

	private DatePickerDialog mDiaPickerDialog;

	@Override
	public void onClick(View v) {
		int vid = v.getId();
		switch (vid) {
		case R.id.id_avart_image:
			// 调用相机
			// Intent intent = new Intent(this, SelectPicActivity.class);
			// startActivityForResult(intent, TO_SELECT_PHOTO);
			break;
		case R.id.set_user_birthday: {
			showDatePicker(); // 调用生日
		}
			break;
		case GPConstants.NAVIGATION_RIGHT_BUTTON: {
			uploadInfo();

			// anys = new AnsyTry(); // 调用上传个人信息接口｛异步上传个人形系｝
			// anys.execute();

		}
			break;
		case R.id.test: {
			dialog(); // 性别选择
		}
			break;
		case R.id.mask: {
			Intent intent2 = new Intent(); // 查看二维码
			intent2.setClass(this, GPMyQRCodeActivity.class);
			startActivity(intent2);
			
		}
			break;
		case GPConstants.NAVIGATION_LEFT_BUTTON: {
//			onBackPressed();
			Intent intent = new Intent(GPUserProfileActivity.this,GPHomeActivity.class);
			startActivity(intent);
			finish();
		}
			break;
		case R.id.set_district:
			// startActivityForResult(new Intent(this,
			// GetAddressInfoActivity.class), 10000);
			break;
		case R.id.checkout_btn: {
			ShowOutapp(); // 退出
		}
			break;
		default:
			break;
		}
	}

	class EditUserIcon extends PopupWindow {
		public EditUserIcon(Context mContext, View parent) {
			View view = View
					.inflate(mContext, R.layout.item_popupwindows, null);
			view.startAnimation(AnimationUtils.loadAnimation(mContext,
					R.anim.fade_ins));
			LinearLayout ll_popup = (LinearLayout) view
					.findViewById(R.id.ll_popup);
			ll_popup.startAnimation(AnimationUtils.loadAnimation(mContext,
					R.anim.push_bottom_in_2));

			setWidth(LayoutParams.FILL_PARENT);
			setHeight(LayoutParams.FILL_PARENT);
			setBackgroundDrawable(new BitmapDrawable());
			setFocusable(true);
			setOutsideTouchable(true);
			setContentView(view);
			showAtLocation(parent, Gravity.BOTTOM, 0, 0);
			update();

			Button bt1 = (Button) view
					.findViewById(R.id.item_popupwindows_camera);
			Button bt2 = (Button) view
					.findViewById(R.id.item_popupwindows_Photo);
			Button bt3 = (Button) view
					.findViewById(R.id.item_popupwindows_cancel);
			bt1.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					takePhoto();
					dismiss();
				}
			});
			bt2.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					slectImageEvent();
					dismiss();
				}
			});
			bt3.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismiss();
				}
			});

		}
	}

	public void slectImageEvent() {
		Intent target = GPFileUtils.createGetImageIntent();
		Intent intent = Intent.createChooser(target, "请选择照片！");
		try {
			startActivityForResult(intent, REQUEST_SELECT_IMAGE);
		} catch (ActivityNotFoundException e) {

		}
	}

	public void takePhoto() {
		PackageManager manager = getPackageManager();
		if (null == manager) {
			Log.e(TAG, "takeImage - getPackageManager FAILED.");
			return;
		}

		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		if (null == intent.resolveActivity(manager)) {
			Log.e(TAG, "takeImage - resolveActivity FAILED.");
			return;
		}

		mImageCaptured = GPExternalStorage.getInstance().createImage();
		if (null == mImageCaptured) {
			Log.e(TAG, "takeImage - create image file FAILED.");
			return;
		}

		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mImageCaptured));
		startActivityForResult(intent, REQUEST_CAPTURE_IMAGE);
	}

	class AnsyTry extends AsyncTask<Void, Integer, Float> {

		@Override
		protected Float doInBackground(Void... arg0) {
			uploadInfo();
			return null;
		}
	}

	private void ShowOutapp() {
		// TODO Auto-generated method stub
		final AlertDialog dlg = new AlertDialog.Builder(this).create();
		dlg.show();
		Window window = dlg.getWindow();
		// *** 主要就是在这里实现这种效果的. 设置窗口的内容页面,shrew_exit_dialog.xml文件中定义view内容
		window.setContentView(R.layout.out_app);
		// 为确认按钮添加事件,执行退出应用操作
		Button ok = (Button) window.findViewById(R.id.checkout_ok);
		ok.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				GPUserModelManager.destroyUserModel(GPUserProfileActivity.this,"usermodel");
				Intent intent = new Intent(GPUserProfileActivity.this,
						GPLoginActivity.class);
				startActivity(intent);
				finish();
				System.exit(0);
			}
		});
		Button cancel = (Button) window.findViewById(R.id.checkout_no);
		cancel.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				dlg.cancel();
			}
		});
		WindowManager.LayoutParams params = dlg.getWindow().getAttributes();
		params.width = params.MATCH_PARENT;
		params.height = 700;
		params.x = 500;
		params.y = 600;
		dlg.getWindow().setAttributes(params);
	}

	private void dialog() {

		AlertDialog menuDialog = new AlertDialog.Builder(this)
				.setTitle("请选择性别")
				.setIcon(R.drawable.ic_launcher)
				.setSingleChoiceItems(new String[] { "男", "女" }, 0,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int pos) {
								// TODO Auto-generated method stub
								if (pos == 0) {
									test.setText("男");
									gender = 0;
									return;
								} else if (pos == 1) {
									test.setText("女");
									gender = 1;
									return;
								}
								dialog.dismiss();
							}
						}).setNegativeButton("确定", null).show();
	}

	private void uploadInfo() {
		// TODO Auto-generated method stub
		// Intent intent2 = this.getIntent();
		// final String sessionId1 = intent2.getStringExtra("sessionId");
		final String nickname = mEt_userNick.getEditableText().toString();// 昵称
		final String signature = mEt_userqm.getEditableText().toString();// 签名
		final String birthday = mBtn_birth.getText().toString();// 生日
		SharedPreferences sp = this.getSharedPreferences("usermodel",
				this.MODE_WORLD_READABLE);
		Editor editor = sp.edit();
		editor.putString("nickname", nickname);
		editor.putString("signature", signature);
		editor.putInt("gender", gender);
		editor.putString("birthday", birthday);
		// editor.putString("district", district);
		editor.commit();
		GPRequestService.get().updateUserinfo(userModel.getSessionId(),
				nickname, signature, birthday, gender, district1,
				new GPClientCallBack() {
					@Override
					public void onSuccess(String result) {
						Map<String, Object> map = GPUtils.getMapForJson(result);
						if (map.containsKey("code")) {
							int code = Integer.parseInt(map.get("code")
									.toString());
							GPErrorStlye
									.stlye(GPUserProfileActivity.this, code);
						}
					}
				});
	}

	public void showDatePicker() {
		final Calendar calendar = Calendar.getInstance();
		mDiaPickerDialog = new DatePickerDialog(
				this,
				new OnDateSetListener() {

					@Override
					public void onDateSet(DatePicker view, int year,
							int monthOfYear, int dayOfMonth) {

						if ((year == calendar.get(Calendar.YEAR)
								&& monthOfYear <= calendar.get(Calendar.MONTH) && dayOfMonth <= calendar
								.get(Calendar.DAY_OF_MONTH))
								|| (year < calendar.get(Calendar.YEAR))) {
							if (monthOfYear > 8) {
								if (dayOfMonth > 9) {
									mBtn_birth.setText(new StringBuilder()
											.append(year).append("-")
											.append(monthOfYear + 1)
											.append("-").append(dayOfMonth));
								} else {
									mBtn_birth.setText(new StringBuilder()
											.append(year).append("-")
											.append(monthOfYear + 1)
											.append("-").append("0")
											.append(dayOfMonth));
								}
							}
							if (monthOfYear < 9) {
								if (dayOfMonth < 10) {
									mBtn_birth.setText(new StringBuilder()
											.append(year).append("-")
											.append("0")
											.append(monthOfYear + 1)
											.append("-").append("0")
											.append(dayOfMonth));
								} else {
									mBtn_birth.setText(new StringBuilder()
											.append(year).append("-")
											.append("0")
											.append(monthOfYear + 1)
											.append("-").append(dayOfMonth));
								}
							}
						}
					}
				}, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
				calendar.get(Calendar.DAY_OF_MONTH));
		mDiaPickerDialog.show();
		mDiaPickerDialog.setCancelable(true);
		mDiaPickerDialog.setTitle("选择生日");
		mDiaPickerDialog.setCanceledOnTouchOutside(true);

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (null != data && resultCode == Activity.RESULT_OK) {
			district.setText(data.getStringExtra("province") + ","
					+ data.getStringExtra("city"));
			// district1 = Integer.parseInt(data.getStringExtra("code"));
			// Log.d(TAG, "district1========"+district1);
		}
		if (resultCode == Activity.RESULT_OK && requestCode == TO_SELECT_PHOTO) {
			picPath = data.getStringExtra(GPSelectPhotoActivity.KEY_PHOTO_PATH);
			Bitmap bm;
			try {
				/*
				 * bm =
				 * BitmapFactory.decodeFile(GPBitmapUtils.getThumbUploadPath(
				 * picPath, 100)); final Bitmap bmBitmap = toRoundBitmap(bm);
				 * Log.d(TAG, "准备上传照片sessionId="+sessionId);
				 * GPRequestService.get().uploadIcon(bm, "jpg", sessionId, new
				 * GPClientCallBack() {
				 * 
				 * @Override public void onSuccess(String result) {
				 * 
				 * Map<String, Object> map = GPUtils.getMapForJson(result);
				 * if(map.containsKey("code")) { int code =
				 * Integer.parseInt(map.get("code").toString()); if (code ==
				 * 200) { imageView.setImageBitmap(bmBitmap); Looper.prepare();
				 * Toast.makeText(GPUserProfileActivity.this, "头像修改保存成功！",
				 * Toast.LENGTH_SHORT).show(); Looper.loop(); } else if(code ==
				 * 21) { Looper.prepare();
				 * Toast.makeText(GPUserProfileActivity.this, "验证过期,请重新登陆！",
				 * Toast.LENGTH_SHORT).show(); Looper.loop(); } else {
				 * Looper.prepare(); Toast.makeText(GPUserProfileActivity.this,
				 * "头像修改保存失败！", Toast.LENGTH_SHORT).show(); Looper.loop(); } } }
				 * });
				 */

			} catch (Exception e) {
				e.printStackTrace();
			}

			if (picPath != null) {
				handler.sendEmptyMessage(TO_UPLOAD_FILE);
			} else {

			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	public void uploadUserInfo() {
		String usernick = mEt_userNick.getEditableText().toString();
		if (usernick == null || usernick.length() == 0) {
			return;
		}
		if (usernick.length() > 32 || usernick.length() < 2) {
			return;
		}
	}

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case TO_UPLOAD_FILE:
				// toUploadFile();
				break;
			case UPLOAD_FILE_DONE:
				break;
			default:
				break;
			}
			super.handleMessage(msg);
		}
	};

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// MoreUserLoginMsg moreUserLoginMsg =new MoreUserLoginMsg(this);
		// moreUserLoginMsg.Dialog();
	}
}
