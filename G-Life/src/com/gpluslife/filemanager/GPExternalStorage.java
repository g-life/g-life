package com.gpluslife.filemanager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Environment;
import android.util.Log;


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.gpluslife.utils.GPConstants;
/**
 * 
 * @author wz
 * @date 2014-9-17
 */
@SuppressLint("NewApi") public class GPExternalStorage {
    private static final String TAG = "GPExternalStorage";
    private static GPExternalStorage mInstance;
    private Context mContext;

    private GPExternalStorage(Context context) {
        mContext = context;
    }

    public static boolean initialize(Context context) {
        assert null != context;

        if (null != mInstance) {
            return true;
        }

        mInstance = new GPExternalStorage(context);

        // 公共图片目录
        if (!createPublicPicturesDir()) {
            return false;
        }

        // 头像目录
        if (null == mInstance.createDir(GPConstants.DIR_NAME_HEAD_IMG)) {
            return false;
        }

        // 缩略图目录
        if (null == mInstance.createDir(GPConstants.DIR_NAME_SCALED_IMAGE)) {
            return false;
        }

        /*// 缩略图目录
        if (null == mInstance.createDir(Constants.DIR_NAME_DOCUMENT)) {
            return false;
        }*/

        return true;
    }

    public static GPExternalStorage getInstance() {
        assert (null != mInstance);
        return mInstance;
    }

    public boolean pathExists(String path) {
        assert null != path;

        if (!isReadable()) {
            return false;
        }

        File file = new File(mContext.getExternalFilesDir(null), path);
        return file.exists();
    }

    public File createDir(String path) {
        assert null != path;

        if (!isReady()) {
            return null;
        }

        File file = new File(mContext.getExternalFilesDir(null), path);
        if (file.exists()) {
            return file;
        }

        if (!file.mkdirs()) {
            return null;
        }

        return file;
    }

    private static boolean createPublicPicturesDir() {
        File dir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);

        if (dir.exists()) {
            return true;
        }

        return dir.mkdirs();
    }

    /*public File getDocument(String hash, String fileName) {
        String path = Constants.DIR_NAME_DOCUMENT + File.separator + hash;
        File file = getFile(path);
        if (!file.mkdirs() && !file.isDirectory()) {
            return null;
        }

        path += File.separator + fileName;
        return getFile(path);
    }*/

    public File getFile(String path) {
        assert null != path;

        if (!isReady()) {
            return null;
        }

        File file = new File(mContext.getExternalFilesDir(null), path);

        return file;
    }

    public File createScaledImage() {
        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(new Date());
        String path = GPConstants.DIR_NAME_SCALED_IMAGE + File.separator + timestamp;
        return getFile(path);
    }

    @SuppressLint("NewApi") public File createImage() {
        if (!isReady()) {
            return null;
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);

        File file;
        try {
            file = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
        } catch (Exception e) {
            Log.e(TAG, "createImage - createTempFile FAILED: " + e.toString());
            return null;
        }

        return file;
    }

    public boolean isReady() {
        return isWritable() && isReadable();
    }

    public boolean isWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public boolean isReadable() {
        String state = Environment.getExternalStorageState();
        return (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state));
    }

    public File getHeadImageDir() {
        return createDir(GPConstants.DIR_NAME_HEAD_IMG);
    }
}
