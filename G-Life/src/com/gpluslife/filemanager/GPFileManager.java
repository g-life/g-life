package com.gpluslife.filemanager;

import java.io.File;

import android.os.Environment;

import com.gpluslife.login.model.GPLoginUserModel;
import com.gpluslife.login.model.GPUserModelManager;

public class GPFileManager {
	
	private  String baseFilePath = Environment.getExternalStorageDirectory().getPath() + File.separator + "Gplus";
//	private  String filePath;
	
	public static GPFileManager getFileManager () {
			return GPHolder.fileManager;
	}
	
	public static class GPHolder
	{
		public static GPFileManager fileManager = new GPFileManager();
	}
	
	public void setBaseFilePath(String baseFilePath)
	{
		
		this.baseFilePath = baseFilePath;
	}

	public String getBaseFilePath()
	{
		
		return baseFilePath;
	}

//	public String getFilePath() {
//		return filePath;
//	}	
	
	public String createFilePath(String fileName)
	{	
		GPLoginUserModel userModel = GPLoginUserModel.get();
		String path = "";
		if (isWriteAndRead()){
			if (fileName == null)
			{
				path = baseFilePath + File.separator + userModel.getUserName() ;
			}
			else {
				path = baseFilePath + File.separator + userModel.getUserName() + File.separator + fileName;
			}
			 
			File file = new File(path);
			if(file.exists() == false)
			{
				file.mkdirs();
			}
		}
		return  path;
	}
	
	public boolean isWriteAndRead(){
		return fileIsWrite() && fileIsOnlyRead();
	}
	
	public boolean fileIsWrite() 
	{
		String stateString = Environment.getExternalStorageState();
		if (stateString.equals(Environment.MEDIA_MOUNTED))
		{
			return true;
		}
		return false;
	}
	
	public boolean fileIsOnlyRead() 
	{
		String stateString = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(stateString) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(stateString))
		{
			return true;
		}
		return false;
	}
	
}
