package com.gpluslife.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.gpluslife.utils.GPSecretUtil;

import android.annotation.SuppressLint;

@SuppressLint("SimpleDateFormat")
public class GPBaseService {

//	static final String url = "http://192.168.2.222:8090/router";//内网
	static final String url = "http://phc.gplus-tech.com/router";//外网

	static final String appKey = "10000";

	static final String secret = "F661DC8AC32D448FAB31C68787497A64";

	static final DateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	Map<String, String> builderBaseMap() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("appKey", appKey);
		map.put("v", "1.0");
		map.put("timestamp", dateFormat.format(new Date()));
		return map;
	}

	Map<String,String> builderSign(Map<String, String> map){
		String sign = GPSecretUtil.sign(map, secret);
		map.put("sign", sign);
		return map;
	}
	
	
}
