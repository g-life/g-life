package com.gpluslife.service.http;

import java.util.Map;


public interface GPBaseCallBack {

	/**
	 * 璋冪敤http涔嬪墠 
	 */
	void onStart();

	/**
	 * 鎴愬姛杩斿洖 
	 * @throws Exception 
	 */
	void onSuccess(String result) throws Exception;

	/**
	 * HTTP 鐘舵�閿欒 
	 */
	void onHttpStatusError(int httpStatus);
	
	/**
	 * 鏈煡閿欒
	 */
	void onError(Throwable ex);

	
}
